<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <title>Renta Car</title>
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/demo.css" rel="stylesheet" />
</head>
<body>
    <div class="wrapper">
        <div class="page-header" style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg')">
            <div class="filter"></div>
            <div class="content-center">
                <div class="content">
                    <div class="motto text-center">
                        <h2 class="title-uppercase text-center">Bienvenido</h2>
                        <br/>
                        <a href="<?php echo base_url();?>car_controller/carrito" class="btn btn-danger btn-round">Vehiculos</a>
                    </div>
                </div>
                <div class="moving-clouds" style="background-image: url('<?php echo base_url()?>assets/img/clouds.png');"></div>
                </div>
            </div>
        </div>
    </a>
</h6>
</div>
</footer>
</body>

</html>

<?php 
$id=$_REQUEST['idcar'];
	?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap.min.js">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  	<link rel="stylesheet" href="/resources/demos/style.css">
  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg')"><br><br><br>
<div class="container">


	<h1 align="center" style="color:#FFFFFF">Arrendamiento de vehiculos</h1><br>
		<form autocomplete="off" action="<?php echo base_url();?>renta_controller/insertar_renta" method="POST">



<center>
	<div class="mx-auto" >
		<div class= "col-md-">
			<label style="color:#FFFFFF">Seleccione el DUI</label>
			<select   name="dui"  id="dui" required class="js-example-basic-single form-control"  onkeyup="lalola();" onchange="lalola();">
				<option>Selecione un DUI</option>
				<?php
				foreach ($dui as $du) { ?>
					<option value="<?=$du->Id_DUI?>"><?=$du->Id_DUI?></option>
					<?php } ?>
 			</select> 
	</div>
		</div>
		<br>
</center>

	<input type="hidden" name="id" value="<?php echo $id ?>"> 
	<input name="vehiculo" type="hidden" class="form-control" value="<?=$vehiculo->Id_vehiculo?>">
		
					<div class="row" >	
						<div class="col-md-6">
				<label style="color:#FFFFFF">Vehiculo</label>
				<input name="no1" type="text" class="form-control" required readonly value="<?=$vehiculo->Modelo?> | <?=$vehiculo->Placa?>" >
	
			</div>
			<div class="col-md-6">
				<label style="color:#FFFFFF">Nombre</label>
				<input name="nombre" id="nombre" type="text" class="form-control" required readonly="">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<label style="color:#FFFFFF">Fecha de reserva</label>
				<input type="text" name="f_reserva" class="form-control" id="datepicker" required>
			</div>		
			<div class="col-md-6">
				<label style="color:#FFFFFF">Hora de reserva</label>
				<input type="time" name="h_reserva" class="form-control"  required>
			</div>
		</div><br>
		
		<div class="row">
			<div class="col-md-6">
				<label style="color:#FFFFFF">Fecha de devolucion</label>
				<input type="text" name="f_devolucion" class="form-control" id="datepicker1"required>
			</div>		
			<div class="col-md-6">
				<label style="color:#FFFFFF">Hora de devolucion</label>
				<input type="time" name="h_devolucion" class="form-control" required>
			</div>
		</div><br>


		<div class="row">
			<div class="col-md-6">
				<label style="color:#FFFFFF">Seleccione el anexo</label>
				<select name="anexo" required class="form-control">
					<option>Seleccione un anexo</option>
					<?php
					foreach ($anexo as $ane) { ?>
						<option value="<?=$ane->Id_anexo?>"><?=$ane->Id_anexo?></option>
					<?php } ?>
				</select>		
		</div>
		<div class="col-md-6">
				<label style="color:#FFFFFF">Ingrese la fianza</label>
				<input type="text" name="fianza" id="fianza" class="form-control"  maxlength="4" id="costo" pattern="[0-9-.-$]+" title="LA fianza no puede ser mayor de 9999" required value="<?=$vehiculo->Fianza?>">
			</div>
		</div><br>



		<div class="row">	
			<div class="col-md-5">
				<label style="color:#FFFFFF">Costo por dia</label>
				<input  type="text" name="costo_alquiler" readonly class="form-control" maxlength="5" id="dia" pattern="[0-9-.-$]+" title="El costo de alquiler no puede ser mayor de 9999" required   value="<?=$vehiculo->Precio_dia?>">
			</div>

	
				<div class="col-md-1">
				<label style="color:#FFFFFF">Horas</label>
				<input type="text" name="hora" class="form-control" id="horas" onkeyup="operacion2();" onchange="operacion2();" min="5" max="23"  title="Eliga un valor mayor de 4 y menor de 24">
			</div>
				<div class="col-md-1">
				<label style="color:#FFFFFF">Dias</label>
				<input type="text" name="dia" class="form-control" id="dias" onkeyup="operacion();" onchange="operacion();" min="1" max="30" title="Eliga un valor mayor de 4 y menor de 24">
			</div>
		
			<div class="col-md-5">
				<label style="color:#FFFFFF">Costo total por alquiler</label>
				<input type="double" name="cdia" class="form-control" maxlength="5" id="costo" pattern="[0-9-.-$]+" title="El costo de alquiler no puede ser mayor de 9999" required readonly>
			</div></div>	
		
		
				<div  style="text-align: right;width:790px" >
				<input type="submit" name="ingresar" class="btn btn-primary" value="Agregar" readonly="">
			
			
</div>
</div>
</form>	
</div>	
</body>
</html>
<script type="text/javascript">
	  $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#datepicker" )
        .datepicker({minDate: 0,
          defaultDate: "+1w",
          changeMonth: true,
         
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#datepicker1" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });

    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
</script>



<script type="text/javascript">
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

</script>


<script type="text/javascript">

			function lalola(){
		var id=$('#dui').val();
		var url='<?php echo base_url();?>renta_controller/llenar_nombre';	
		$.ajax({
		url:url,
		data:'Id_DUI='+id,
		type:'POST',
		success:function(respuesta){
			$('#nombre').val(respuesta);

		}
			});

	}


</script>

<script type="text/javascript">
	
		function operacion() {
 
  var dias =$('#dias').val();
  var dia =$('#dia').val();;

  var total=dias*dia;
   $("#costo").val(total.toFixed(2));


}
		
</script>

<script type="text/javascript">
	
		function operacion2() {
  var dias =$('#dias').val();
  var horas =$('#horas').val();
  var dia =$('#dia').val();;
  var lolo = 24;


  var total=dia/lolo*horas;
  var total1=dias*dia;
  var dh=total+total1; 
     $("#costo").val(dh.toFixed(2));

}
		
</script>
<?php 
$idcar=$_REQUEST["idcar"];
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>		
	</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap.min.js">
</head>
<body class="bodya" style="color:white">
	<br><br><br><br>
	
	<div class="card-signin2 col-md-10 mx-auto my-5">
	<div class="container my-5">
	<div align="center" class="my-5"><h1>ACTUALIZAR AUTO</h1></div>
	
	
 
		<form  action="<?php echo base_url();?>car_controller/actuali_car" method="POST">

		<div class="row ">
			<div class="col-md-4">
				<input type="hidden" name="id_car" value="<?=$car->Id_vehiculo;?>" >
				<select name="marca" required class="form-control" required>
					<option> marca del auto</option>
					<?php foreach ($marca as $d) {?>
				<option value="<?=$d->Id_marca?>"<?=$d->Id_marca==$car->Marca_id ?'selected':'';?>><?=$d->Marca;?></option>
						<?php } ?>
					</select>
			</div>	
			<div class="col-md-4">
                <select name="tipo_auto" required class="form-control" required> 
                    <option>seleccione el tipo de auto</option>
                    <?php foreach ($tip as $t) {?>                
            <option value="<?=$t->Id_tipo_auto?>"<?=$t->Id_tipo_auto==$car->Tipo_auto_id ?'selected':'';?>><?=$t->TIpo_auto ?></option>
                <?php } ?>
                </select>
            </div>
				 <div class="col-md-4">
            	<input type="text" name="fianza" class="form-control"  maxlength="4" pattern="[0-9]+" title="Solo números" placeholder="Digite la cantidad de fianza" required value="<?=$car->Fianza?>">
            </div>	
			</div><br>

			<div class="row ">
	<div class="col-md-4">				
				<input type="text" name="anio" required min="2009"  value="<?=$car->Anio?>" placeholder="Digite el año del auto" class="form-control" value="">
			</div>
		<br>
				
			<div class="col-md-4">
				<input type="text" name="placa" required value="<?=$car->Placa?>" placeholder="nuemero de placa" class="form-control" required >
			</div>
				<div class="col-md-4">
				<input type="text" name="modelo" required placeholder="modelo del vehiculo" value="<?=$car->Modelo?>" class="form-control">  
			</div>		
			
		</div>
		<br>		
		<div class="row">
			<div class="col-md-4">
			<input type="text" name="color" value="<?=$car->Color?>" required placeholder="color segun tarjeta de circulacion" class="form-control">
			</div>

			<div class="col-md-4">
				<input type="text" name="precio_dia" value="<?=$car->Precio_dia?>" placeholder="precio de alquiler por dia" required class="form-control">
			</div>
			<div class="col-md-4">
				<select class="form-control" name="estado_auto" required >
					<option>selecione el estado del auto</option>
					<?php foreach ($est as $d) {?>
						<option value="<?=$d->Id_estado_vehiculo?>"<?=$d->Id_estado_vehiculo==$car->Estado_id ?'selected':'';?>><?=$d->Estado_vehiculo;?></option>
						<?php } ?>
				</select>
			</div>
		</div><br>

		<div class="row">
              <div class="col-md-4">				
				<select class="form-control" name="caracteris" required>
					<option>
						<table >
					 <tr>
						<td>Aire condicionado|</td>
						<td>Combustible|</td>
						<td>Transmision|</td>
						<td>Capacidad</td>
					</tr>
				</table>
			</option>
		        <?php foreach ($cart as $c) {?>
				<option value="<?=$c->idcaracteristica?>"<?=$c->idcaracteristica==$car->caracteristica_id ?'selected':'';?>><?=$c->aire?> | <?=$c->combustible?> | <?=$c->transmision?> | <?=$c->capacidad?></option>
			<?php } ?>
		
				</select>
			</div>
	
			
		<div class="col-md-4">
			Seleccione una imagen 
				<input type="file"  accept=" image/jpeg,image/jpg" title="<?=$car->url_imagen?>" name="foto"  class="form-control" placeholder="Seleccione una imagen">
			</div>
			<div class="col-md-4">
			<img src="<?php echo base_url()?>assets/img/<?=$car->url_imagen?>" heigth="500px" width="200px" title="<?=$car->url_imagen?>" value="<?=$car->url_imagen?>">
		</div>		  
		</div>
	

		<br>		
		<br>
		<div class="row offset-9">
			<div class="col-md-2" >
				<input type="submit" name="recop" value="Guardar Cambios" class="btn btn-primary">
			</div>
		</div>
		</form>
	</div>

</div>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
	<title>Usuario</title>
  <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style.css">

</head>
<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg'); color: white; font-family: comic sans ms; background-attachment: fixed;">
  <br>
  <div class="container-fluid">

    <div class="offset-5 my-5">
      <font size="+5">Usuarios</font>
    </div>
    <div class="container">
      <form>
        <div class="row">
          <div class="col-md-4">
            <input id="buscar" class="form-control" type="text" placeholder="Buscar">
          </div>
          <div class="offset-4 col-md-4">
            <a type="button" href="<?php echo base_url();?>login_controller/barra_admin" class="btn btn-success">Agregar Usuario</a>
          </div>
        </div>
      </form>
    </div>
    <br>
      <table class="table table-dark table-hover header_fijo" id="tabla" border="3">
       <thead >
        <th>Nombre</th>
        <th>Usuario</th>
        <th>Rol</th>
        <th>Correo</th>
        <th>Actualizar</th>
        <th>Eliminar</th>
      </thead>
      <?php
      foreach ($usuario as $l){ ?>
        <tr>
         <td><?php echo $l->nombre_usuario ?></td>
         <td><?php echo $l->usuario ?></td>
         <td><?php echo $l->rol ?></td>
         <td><?php echo $l->correo ?></td>
         <td><a href="<?php echo base_url()?>login_controller/accion_actualizar?id=<?=$l->idusuario?>" class="btn btn-info btn-round">Actualizar</a></td>
         <?php if($l->usuario!=$this->session->userdata('usuario')){?>
           <td><a href="<?php echo base_url()?>login_controller/eliminar?id=<?=$l->idusuario?>" class="btn btn-danger btn-round" onclick=" return confirm('Estas seguro que quieres eliminar el registro del usuario: <?=$l->nombre_usuario?>?');">Eliminar</a></td>
         <?php } ?>
       </tr>
     <?php } ?>		
   </table>

 <br>
</div>
</body>
</html>
<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>
<script type="text/javascript">
  $('th').click(function() {
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc) {
      rows = rows.reverse()
    }
    for (var i = 0; i < rows.length; i++) {
      table.append(rows[i])
    }
    setIcon($(this), this.asc);
  })

  function comparer(index) {
    return function(a, b) {
      var valA = getCellValue(a, index),
      valB = getCellValue(b, index)
      return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
    }
  }

  function getCellValue(row, index) {
    return $(row).children('td').eq(index).html()
  }

  function setIcon(element, asc) {
    $("th").each(function(index) {
      $(this).removeClass("sorting");
      $(this).removeClass("asc");
      $(this).removeClass("desc");
    });
    element.addClass("sorting");
    if (asc) element.addClass("asc");
    else element.addClass("desc");
  }
</script>
<script language="javascript">
  var busqueda = document.getElementById('buscar');
  var table = document.getElementById("tabla").tBodies[0];

  buscaTabla = function(){
    texto = busqueda.value.toLowerCase();
    var r=0;
    while(row = table.rows[r++])
    {
      if ( row.innerText.toLowerCase().indexOf(texto) !== -1 )
        row.style.display = null;
      else
        row.style.display = 'none';
    }
  }
  busqueda.addEventListener('keyup', buscaTabla);
</script>
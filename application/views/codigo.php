<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body style="color: white">
	<br>
	<div class="container">
		<form id="formu2" action="<?php echo base_url();?>login_controller/validar_codigo" method="post" autocomplete="off">
			<br><br><br><br><br>
			<div class="card col-md-7 mx-auto my-5">
				<div class="my-5">
					<center><h2>Verificar Código</h2></center>
				</div>
				<div class="col-md-12">
					Ingrese su código de verificación:
					<input id="codigo" type="text" name="codigo" class="form-control" maxlength="6" required>
					<br>
					<div class="row">
						<div class="offset-2 col-md-3">
							<center><a href="<?php echo base_url()?>login_controller/index" class="btn btn-success"
								>Inicio</a></center>
							</div>
							<div class="col-md-4">
								<button class="btn btn-primary" id="enviar" type="button" onclick="validar_codigos()">Siguiente</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</body>
	</html>
	<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
	<script type="text/javascript">
		function validar_codigos()
		{
			var codigo=$('#codigo').val();
			var url = '<?php echo base_url(); ?>Login_controller/validar_codigos';
			$.ajax({
				url:url,
				data:'codigo='+codigo,
				type:'post',
				success: function(val){
					if(val==0){
						alert('Codigo Incorrecto');
						
					}else{
						$('#formu2').submit();
					}
				}
			});
		}
	</script>
	<script>
		var codigoo = document.getElementById("codigo");
		codigoo.addEventListener("keyup", function(event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("enviar").click();
			}
		});
	</script>
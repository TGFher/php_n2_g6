<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
  <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>
  <link href="<?php echo base_url()?>assets/css/demo.css" rel="stylesheet" />

<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg'); color: white; font-family: comic sans ms; background-attachment: fixed;">

	<div class="container">
		<div class="offset-5 my-5 mx-auto">
			<center>Registro de clientes</center>
		</div>
		<table class="table table-dark table-hover text-center">
			<tr>
				<th><font color="white">Tipo de documento</font></th>
				<th><font color="white">Documento</font></th>
				<th><font color="white">Nombre</font></th>
				<th><font color="white">Apellido</font></th>
				<th><font color="white">Edad</font></th>
				<th><font color="white">Fecha de nacimiento</font></th>
				<th><font color="white">Sexo</font></th>
				<th><font color="white">Tipo de licencia</font></th>
				<th><font color="white">Numero de licencia</font></th>
				<th><font color="white">Pais</font></th>
				<th><font color="white">Telefono</th>
				<th><font color="white">Telefono de referencia</font></th>
			</tr>

			<?php foreach ($cliente as $cl){?>
				<tr>
					<td><?php echo $f->Documento?></td>
					<td><?php echo $f->Id_DUI?></td>
					<td><?php echo $f->Nombre?></td>
					<td><?php echo $f->Apellido?></td>
					<td><?php echo $f->Edad?></td>
					<td><?php echo $f->Fnacimiento?></td>
					<td><?php echo $f->Sexo?></td>
					<td><?php echo $f->Licencia?></td>
					<td><?php echo $f->Numero_licencia?></td>
					<td><?php echo $f->Pais?></td>
					<td><?php echo $f->Telefono?></td>
					<td><?php echo $f->Telefono_referencia?></td>
					<td><a href="<?php echo base_url()?>cliente_controller/llenar_cliente?id=<?=$cl->Id_DUI?>" class="btn btn-success"></a></td>
					<td><a href="<?php echo base_url()?>cliente_controller/eliminar_cliente?id=<?=$cl->Id_DUI?>" class="btn btn success"></a></td>
					
				</tr>


				<?php}?>

			
		</table>

	


	</div>

</body>
</html>
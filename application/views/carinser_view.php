<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>		
	</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap.min.js">
</head>
<body class="bodya" style="font-family: comic sans ms;color: white; background-attachment:">
	<br><br><br><br>
	 
	


<!-- Modal -->
<div class="modal fade" id="marca" tabindex="-1" role="dialog" aria-labelledby="marca" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="marca">Nueva Marca</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container my-5">
        	<form action="<?php echo base_url()?>car_controller/ingremarc" method="POST">
        		
        		<div class="col-md-4">
        			<li>Digite la Nueva Marca</li>
        			<input type="text" name="marca" required class="form-control" maxlength="15">
        		</div>
        		

        	<div class="row offset-5">
        		<a href="<?php echo base_url()?>car_controller/mttomarca">Mantenimiento de marcas</a>
        	</div>	
        	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <div class="row"><input type="submit" name="recop" value="Guardar" class="btn btn-primary"></div></form>
      </div>
    </div>
  </div>
</div>
	
	<div class="card-signin2 col-md-10 mx-auto my-5">
		
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#marca">
  ingresar nueva marca
</button>



		<div align="center" class="my-5"><h1 style="size: +2">Registro de Auto</h1></div>
		
 <div class="container">
		<form  action="<?php echo base_url();?>car_controller/insertar_auto" method="POST" autocomplete="off">

		<div class="row ">
			<div class="col-md-4">
				<select name="marca" required class="form-control" required>
					
					<option disabled selected> Marca del auto</option>
					<?php 
					foreach ($mar as $m) { ?>
						<option value="<?=$m->Id_marca?>"><?=$m->Marca ?></option>
					<?php } ?>					
				</select>
			</div>	
			<div class="col-md-4">
                <select name="tipo_auto" required class="form-control" required> 
                    <option disabled selected>Seleccione el tipo de auto</option>
                    <?php foreach ($tipo as $t) {?>                
                    <option value="<?=$t->Id_tipo_auto?>"><?=$t->TIpo_auto?></option>
                <?php } ?>
                </select>
            </div>
            <div class="col-md-4">
            	<input type="text" name="fianza" class="form-control"  maxlength="4" pattern="[0-9]+" title="Solo números" placeholder="Digite la cantidad de fianza" required>
            </div>	
			</div>	
		<br>
		<div class="row ">
			<div class="col-md-4">				
				<input type="text" name="anio" required min="2009"   placeholder="Digite el año del auto" class="form-control"  maxlength="4" pattern="[0-9]+" title="Solo números">
			</div>
			<div class="col-md-4">
				<input type="text" name="placa" required  placeholder="Número de placa" class="form-control" maxlength="7" title="sin guion">
			</div>
				<div class="col-md-4">
				<input type="text" name="modelo" required placeholder="Modelo del Vehiculo" class="form-control">  
			</div>		
		</div>
		<br>		
		<div class="row">
			<div class="col-md-4">
				<input type="text" name="color" pattern="[a-Z]" required placeholder="Color según tarjeta de circulacion" class="form-control">
			</div>
<div class="col-md-4">
				<input type="text" name="precio_dia" pattern="[0-9-.]+" placeholder="Alquiler por dia" required class="form-control" min="0" maxlength="5">
			</div>
			<div class="col-md-4">
				<select class="form-control" name="estado_auto" required readonly>
					<option value="2">Disponible</option>
				</select>
			</div>
						
		</div>
		<br>
		<div class="row ">
			<div class="col-md-6">
				<select class="form-control" name="caracteris" required>
					<option disabled selected>Caracteristicas del auto</option>
					<option value="" disabled>
						<table >
					 <tr>
						<td>Aire condicionado|</td>
						<td>Combustible|</td>
						<td>Transmision|</td>
						<td>Capacidad</td>
					</tr>
				</table>
			</option>
		        <?php foreach ($caract as $c) {?>
				<option value="<?=$c->idcaracteristica?>"><?=$c->aire?>  | <?=$c->combustible?>  | <?=$c->transmision?> | <?=$c->capacidad?></option>
			<?php } ?>
				</select>
			</div>
			<div class="col-md-6">
				<input type="file"  accept="image/jpeg" name="foto" reqired  class="form-control" placeholder="seleccione una imagen">
			</div>
		</div>
		<br>		
		<br>
		<div class="row">
			<div class="col-md-2 offset-8" >
				<input type="submit" name="recop" value="Agregar" class="btn btn-primary">
			</div>
		</div>
		</form>
	</div>
</div>
	<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
</body>
</html>
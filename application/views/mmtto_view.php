<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap.min.js">
</head>
<body>
	<br><br><br><br>
<div class=" container my-5">
	<table class="table">
		<thead>
			<th hidden>ID</th>
			<th>Marca</th>
			<th>Actualizar</th>
			<th>Eliminar</th>
		</thead>
<?php foreach ($vid as $v ) { ?>
	<tr>
		<td hidden><?php echo $v->Id_marca ?></td>
		<td><?php echo $v->Marca ?></td>
		<td><!-- Modal -->
<div class="modal fade" id="<?=$v->Id_marca?>" tabindex="-1" role="dialog" aria-labelledby="<?=$v->Id_marca?>" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="<?=$v->Id_marca?>">Actualizar Marca</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container my-5">
        	<form action="<?php echo base_url()?>car_controller/actum?idm=<?=$v->Id_marca?>" method="POST">
        		
        		<div class="col-md-4">
        			<li>actualice la Marca</li>
        			<input type="text" name="marca1" required class="form-control" maxlength="15" value="<?=$v->Marca?>">
        		</div>
        		       	
        	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <div class="row"><input type="submit" name="recop" value="Guardar" class="btn btn-primary"></div></form>
      </div>
    </div>
  </div>
</div>
	
		
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#<?=$v->Id_marca?>">
  actualizar</button></a></td>

		<td><button class="btn btn-primary"><a href="<?php echo base_url()?>car_controller/elimar?idm=<?=$v->Id_marca?>">Eliminar</a> </button></td>
	</tr>
<?php } ?>

	</table>
</div>


<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>

</body>
</html>
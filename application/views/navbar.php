<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/demo.css" rel="stylesheet" />

<?php if($this->session->userdata('rol_id')=='1') { ?>
    <nav class="navbar navbar-expand-md fixed-top navbar-transparent" color-on-scroll="500">
        <div class="container">
            <div class="navbar-translate">
                <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar"></span>
                    <span class="navbar-toggler-bar"></span>
                    <span class="navbar-toggler-bar"></span>
                </button>
                <a class="navbar-brand"  href="<?php echo base_url()?>login_controller/index">Rent Car</a>

            </div>
            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav ml-auto">
                
                <li class="nav-item">
                        <a href="<?php echo base_url()?>cliente_controller/index" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Clientes</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url()?>login_controller/mostrar_usuario" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Usuarios</a>
                    </li>

                    <li class="nav-item">
                        <a href="<?php echo base_url()?>login_controller/barra_admin" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Agregar usuario</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url()?>car_controller/marca" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Agregar vehiculo</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url()?>car_controller/most_car" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Inventario</a>
                    </li>

                     <li class="nav-item">
                        <a href="<?php echo base_url()?>renta_controller/mostrar_renta" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Ver renta</a>
                    </li>

                     <li class="nav-item">
                        <a href="<?php echo base_url()?>car_controller/carrito" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>carrito</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url()?>car_controller/stock_carr" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>stock</a>
                    </li>

                    <li class="nav-item">
                      <a href="<?php echo base_url()?>login_controller/cerrar_sesion" class="btn btn-danger btn-round">Salir</a>
                  </li>
              </ul>
          </div>
      </div>
  </nav>
<?php }elseif($this->session->userdata('rol_id')=='2'){?>
<nav class="navbar navbar-expand-md fixed-top navbar-transparent" color-on-scroll="500">
        <div class="container">
            <div class="navbar-translate">
                <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar"></span>
                    <span class="navbar-toggler-bar"></span>
                    <span class="navbar-toggler-bar"></span>
                </button>
                <a class="navbar-brand"  href="<?php echo base_url()?>login_controller/index">Rent Car</a>

            </div>
            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="<?php echo base_url()?>car_controller/most_car" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Inventario</a>
                    </li>

                    <li class="nav-item">
                        <a href="<?php echo base_url()?>renta_controller/wacha" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Renta</a>
                    </li>

                     <li class="nav-item">
                        <a href="<?php echo base_url()?>renta_controller/mostrar_renta" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>Ver renta</a>
                    </li>

                     <li class="nav-item">
                        <a href="<?php echo base_url()?>car_controller/carrito" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>carrito</a>
                    </li>
                    <li class="nav-item">
                      <a href="<?php echo base_url()?>login_controller/cerrar_sesion" class="btn btn-danger btn-round">Salir</a>
                  </li>
              </ul>
          </div>
      </div>
  </nav>
<?php }else{} ?>

</body>
</html>


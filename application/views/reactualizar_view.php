<?php 
$id=$_REQUEST['id'];
	?>
	<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap.min.js">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  	<link rel="stylesheet" href="/resources/demos/style.css">
  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg')"><br><br><br>
<div class="container">


	<h1 align="center" style="color:#FFFFFF">Arrendamiento de vehiculos</h1><br>
		<form autocomplete="off" action="<?php echo base_url();?>renta_controller/actualizar_renta" method="POST">


<center>
	<div class="mx-auto" >
		<div class= "col-md-">
			<label style="color:#FFFFFF">Seleccione el DUI</label>
			<select   name="dui"  id="dui" required class="js-example-basic-single form-control" onchange="lalola()">
				<option>Seleccione un DUI</option>
				<?php
				foreach ($dui as $du) { ?>
					<option value="<?=$du->Id_DUI?>"<?=$du->Id_DUI==$renta->DUI_id ?'selected':'';?>><?=$du->Id_DUI;?></option>
					<?php } ?>
 			</select> 
		</div>
		</div><br>
		</center>

		<div class="row" >	
			<div class="col-md-6">
				<label style="color:#FFFFFF">Seleccione el vehiculo</label>
				<select name="vehiculo" required class="form-control">
					<option>Seleccione un vehiculo</option>
					<?php
					foreach ($vehiculo as $ve) { ?>
						<option value="<?=$ve->Id_vehiculo?>"<?=$ve->Id_vehiculo==$renta->Vehiculo_id ?'selected':'';?>><?=$ve->Marca_id?>/<?=$ve->Modelo?>/<?=$ve->Color;?></option>
					<?php } ?>
				</select>		
			</div>

	<input type="hidden" name="id" value="<?php echo $id ?>">	

		
			<div class="col-md-6">
				<label style="color:#FFFFFF">Fecha de reserva</label>
				<input type="text" name="f_reserva" class="form-control" id="datepicker" required value="<?=$renta->Fecha_reserva?>">
			</div>
			</div>

			<div class="row">
			<div class="col-md-6">
				<label style="color:#FFFFFF">Hora de reserva</label>
				<input type="time" name="h_reserva" class="form-control" required value="<?=$renta->Hora_reserva?>">
			</div>
			<div class="col-md-6">
				<label style="color:#FFFFFF">Fecha de devolucion</label>
				<input type="text" name="f_devolucion" class="form-control" id="datepicker1"required value="<?=$renta->Fecha_devolucion?>">
			</div>
			</div><br>

			<div class="row">
			<div class="col-md-6">
				<label style="color:#FFFFFF">Hora de devolucion</label>
				<input type="time" name="h_devolucion" class="form-control" required value="<?=$renta->Hora_devolucion?>">
			</div>
			<div class="col-md-6">
				<label style="color:#FFFFFF">Seleccione el anexo</label>
				<select name="anexo" required class="form-control">
					<option>Seleccione un anexo</option>
					<?php
					foreach ($anexo as $ane) { ?>
						<option value="<?=$ane->Id_anexo?>"<?=$ane->Id_anexo==$renta->Anexo_id ?'selected':'';?>><?=$ane->Id_anexo;?></option>
					<?php } ?>
				</select>		
		</div>
	</div>

		<div class="row">
		<div class="col-md-6">
				<label style="color:#FFFFFF">Ingrese la fianza</label>
				<input type="text" name="fianza" id="fianza" class="form-control"  maxlength="4" id="costo" pattern="[0-9-.-$]+" title="LA fianza no puede ser mayor de 9999" required value="<?=$renta->fianza?>">
			</div>	
	
			<div class="col-md-6">
				<label style="color:#FFFFFF">Costo por dia</label>
				<input type="text" name="costo_alquiler"  class="form-control" maxlength="4" id="dia" pattern="[0-9-.-$]+" title="El costo de alquiler no puede ser mayor de 9999" required value="<?=$renta->costo_alquiler?>">
			</div>
		</div><br>

			<div class="row">
			
				<div class="col-md-3">
				<label style="color:#FFFFFF">Horas</label>
				<input type="text" name="hora" class="form-control" id="horas" onkeyup="operacion2();" onchange="operacion2();" value="<?=$renta->Hora?>">
			</div>
				<div class="col-md-3">
				<label style="color:#FFFFFF">Dias</label>
				<input type="text" name="dia" class="form-control" id="dias" onkeyup="operacion();" onchange="operacion();" value="<?=$renta->Dia?>">
			</div><div class="col-md-6">
				<label style="color:#FFFFFF">Costo total por alquiler</label>
				<input type="text" name="cdia" class="form-control" maxlength="4" id="costo" pattern="[0-9-.-$]+" title="El costo de alquiler no puede ser mayor de 9999" required value="<?=$renta->Costo_total?>">
			</div>
		</div><br>
		
			
		
		
				<div  style="text-align: right;width:790px" >
				<input type="submit" name="ingresar" class="btn btn-primary" value="Agregar">
			
			
</div>
</div>
</form>	
</div>	
</body>
</html>
<script type="text/javascript">
	  $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#datepicker" )
        .datepicker({minDate: 0,
          defaultDate: "+1w",
          changeMonth: true,
         
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#datepicker1" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });

    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
</script>

<script type="text/javascript">

     $('#fianza').mask('$0000');



     $('#costo').mask('$0000');

     

</script>




<script type="text/javascript">
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

</script>


<script type="text/javascript">

			function lalola(){
		var id=$('#dui').val();
		var url='<?php echo base_url();?>renta_controller/llenar_nombre';	
		$.ajax({
		url:url,
		data:'Id_DUI='+id,
		type:'POST',
		success:function(respuesta){
			$('#nombre').val(respuesta);

		}
			});

	}


</script>

<script type="text/javascript">
	
		function operacion() {
 
  var dias =$('#dias').val();
  var dia =$('#dia').val();;

  var total=dias*dia;
   $("#costo").val(total);

}
		
</script>

<script type="text/javascript">
	
		function operacion2() {
 
  var horas =$('#horas').val();
  var dia =$('#dia').val();;

  var total=dias*horas;
   $("#costo").val(total);

}
		
</script>


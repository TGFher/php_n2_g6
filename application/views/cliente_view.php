<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap.min.js">

<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg')"><br><br><br>

	<h1 align="center" style="color: #FFFFFF">Ingreso de nuevos clientes</h1>
	<form autocomplete="off" action="<?php base_url();?>cliente_controller/insertar_cliente" method="POST"></form>

	<div class="container">

		<div class="row">
			<div class="col-md-6">
				<label>Ingrese el numero de documento </label>
				<input type="text" name="Id_DUI" required>
			</div>
			<div class="col-md-6">
				<label>Seleccione el tipo de documento dado</label>
				<select name="Documento_id" required>
					<option value >Seleccione un tipo</option>
					<?php foreach ($documento as $do){ ?>
						<option value="<?=$do->Documento_id?>"><?=$do->Documento?></option>
						<?php}?>					
				</select>				
			</div>			
		</div>

		<div class="row">
			<div class="col-md-6">
				<label>Ingrese el nombre </label>
				<input type="text" name="Nombre" required>
			</div>
			<div class="col-md-6">
				<label>Ingrese el apellido</label>
				<input type="text" name="Apellido">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<label>Ingrese la edad</label>
				<input type="text" name="Edad" required>
			</div>
			<div class="col-md-6">
				<label>Ingrese la fecha de nacimiento</label>
				<input type="Date" name="Fnacimiento">
			</div>		
		</div>

		<div class="row">
			<div class="col-md-6">
				<label>Seleccione el sexo del cliente</label>
				<select name="Sexo_id" required>
					<option>Seleccione un sexo</option>
					<?php foreach ($sexo as $se){?>
						<option value="<?=$se->Sexo_id?>"><?=$se->Sexo?></option>
					<?php}?>					
				</select>				
			</div>
			<div class="col-md-6">
				<label>Ingrese el tipo de licencia</label>
				<select name="Tipo_licencia_id" required="">
					<option>Seleccione un tipo de licencia</option>
					<?php foreach ($tipo as $ti){?>
						<option value="<?=$ti->Tipo_licencia_id?>"><?=$ti->Licencia?></option>
					<?php}?>	
				</select>			
			</div>			
		</div>

		<div class="row">
			<div class="col-md-6">
				<label>Ingrese el numero de licencia</label>
				<input type="text" name="Numero_licencia" required>
			</div>
			<div class="col-md-6">
				<label>Ingrese el pais</label>
				<select name="Pais_id" required>
					<option>Selecione el pais</option>
					<?php foreach($pais as $pa){?>
						<option value="<?=$pa->Pais_id?>"><?=$pa->Pais?></option>
						<?php}?>			
				</select>
			</div>		
		</div>

		<div class="row">
			<div class="col-md-6">
				<label>Ingrese el numero de telefono</label>
				<input type="text" name="Telefono" required>
			</div>
			<div class="col-md-6">
				<label>Ingrese el telefono de referencia</label>
				<input type="text" name="Telefono_referencia">
			</div>		
		</div>
	</div>
</body>
</html>
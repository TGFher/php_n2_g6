<?php 
$id=$_REQUEST['id'];
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style.css">
</head>
<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg'); background-attachment: fixed; font-family: Comic Sans Ms; color: white">
	<form name="editar" action="<?php echo base_url()?>login_controller/actualizar" method="post">
		<div class="container">
			<br><br><br>
			<div class="card-signin2 col-md-10 mx-auto my-5">
				<center><h1>Actualizar Usuario</h1></center>
				<div class="row">
					<div class="offset-1 col-md-5">
					Nombre del Usuario:</td>
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<input type="text" id="nombre" name="nombre" value="<?=$usuario->nombre_usuario?>" required class="form-control" onkeyup="convertir(2)">
				</div>
				<div class="col-md-5">
					Usuario:
					<input type="text" id="usuario" name="usuario" value="<?=$usuario->usuario?>" class="form-control" required onkeyup="minus()" onblur="validar_user()" onkeypress="if(event.keyCode==32)event.returnValue=false;">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="offset-1 col-md-5">
					Contraseña:
					<input type="password" id="clave1" name="clave" value="<?=$usuario->registro?>" class="form-control" required onkeypress="if(event.keyCode==32)event.returnValue=false;">
					<input type="checkbox" id="mostrar_clave" title="Clic para mostrar clave">
				</div>
				<div class="col-md-5">
					Contraseña:
					<input type="password" id="clave2" name="registro" value="<?=$usuario->registro?>" class="form-control" required onkeypress="if(event.keyCode==32)event.returnValue=false;">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="offset-1 col-md-5">
					Rol:
					<select name="rol" class="form-control" required>
						<?php foreach ($rol as $d) {?>
						<option value="<?=$d->idrol?>"<?=$d->idrol==$usuario->rol_id ?'selected':'';?>><?=$d->rol;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-5">
					Correo Eléctronico:
					<input type="email" id="correo" name="correo" value="<?=$usuario->correo?>" class="form-control" onblur="validar_mail()">
				</div>
			</div>
			<div class="my-4">
				<center><input type="submit" value="Guardar" class="btn btn-warning btn-round"></center>
			</div>
		</div>
	</div>
</form>
</body>
</html>
<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>
<script type="text/javascript"> 
	window.onload = function () {
		document.getElementById("clave1").onchange = validatePassword;
		document.getElementById("clave2").onchange = validatePassword;
	}
	function validatePassword(){
		var pass2=document.getElementById("clave2").value;
		var pass1=document.getElementById("clave1").value;
		if(pass1!=pass2)
			document.getElementById("clave1").setCustomValidity("Las contraseñas no coinciden");
		else
			document.getElementById("clave1").setCustomValidity('');
	}
	function minus(){
		var nombre=$("#usuario").val();

		$("#usuario").val(nombre.toLowerCase());
	}
	function validar_user()
	{
		var user=$('#usuario').val();
		var url = '<?php echo base_url(); ?>Login_controller/validar_user';
		$.ajax({
			url:url,
			data:'user='+user,
			type:'post',
			success: function(data){
				if(data==1){
					alert('Usuario Ya Registrado');
				}else{
					$('#usuario').addClass('good');

				}
			}
		});
	}
	function validar_mail()
	{
		var correo=$('#correo').val();
		var url = '<?php echo base_url(); ?>Login_controller/validar_mail';
		$.ajax({
			url:url,
			data:'correo='+correo,
			type:'post',
			success: function(val){
				if(val==1){
					alert('Correo Ya Registrado');
				}else{
					$('#correo').addClass('good');

				}
			}
		});
	}
	function convertir(tipo)
	{
		var nombre = document.getElementById("nombre");
		var texto = nombre.value;
		if (tipo == 2){
			nombre.value = texto.toLowerCase();
			nombre.value = nombre.value.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g,
				function($1)
				{
					return $1.toUpperCase(); 
				});
		}
	}
	$(document).ready(function () {
        $('#mostrar_clave').click(function () {
          if ($('#mostrar_clave').is(':checked')) {
            $('#clave1').attr('type', 'text');
          } else {
            $('#clave1').attr('type', 'password');
          }
        });
      });
</script>
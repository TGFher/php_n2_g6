
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in yssour HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>style.css">
</head>
<body>
	<div class="container">
		<div class="d-flex justify-content-center h-100">
			<div class="card">
				<div class="card-header">
					<h3>Iniciar Sesión</h3>
					<div class="d-flex justify-content-end">
						<center><span><i><img src="<?php echo base_url()?>imagen/car.png" width="350px" height="70px"></i></span></center>
					</div>
				</div>
				<div class="card-body">
					<form id="formu" action="<?php echo base_url();?>Login_controller/validar" autocomplete="off" method="post">
						<div class="input-group form-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
							<input type="text" id="usuario" class="form-control" required name="usuario" placeholder="Usuario" maxlength="15" minlength="5" pattern="[a-z-0-9]+" title="Solo minusculas y números" onkeyup="mayus()" onblur="validar_user()" autofocus>

						</div>
						<div class="input-group form-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
							<input type="password" id="clave" name="clave" class="form-control" placeholder="Contraseña" required>
						</div>
						<div class="form-group">
							<input type="submit" id="button" value="Login" class="btn float-right login_btn">
						</div>
					</form>
				</div>

				<div class="card-footer">
					<div class="d-flex justify-content-center">
						<a href="<?php echo base_url()?>Login_controller"  data-toggle="modal" data-target="#exampleModal"><font size="+1.5" color="white">Olvidaste la contraseña?</font></a><br>

						
						<form id="formu2" action="<?php echo base_url();?>Login_controller/recuperar_clave" method="POST" autocomplete="off">
							<div class="container">
								<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Recuperacion de contraseña</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">

												<div class="form-group">
													<label>Ingrese su Correo de Recuperación</label>

													<input type="email" id="correo" name="correo" placeholder="aaaaaaaa@gmail.com"  class="form-control" ><br><br>

													<button class="btn btn-outline-success" id="enviar" type="button" onclick="validar_mail()">Enviar</button>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	</body>
	</html>
	<script type="text/javascript">
		function validar_user()
		{
			var user=$('#usuario').val();
			var url = '<?php echo base_url(); ?>Login_controller/validar_user';
			if(user){
				$.ajax({
					url:url,
					data:'user='+user,
					type:'post',
					success: function(data){
						if(data==1){
						}else{
							alert('Usuario No Registrado');
							$('#usuario').val('');
						}
					}
				});
			}
		}
		function validar_clave()
		{
			var clave=$('#clave').val();
			var url='<?php echo base_url();?>login_controller/validar_clave';
			if(clave)
			{
				$.ajax({
					url: url,
					data:$('#formu').serialize(),
					type: 'post',
					success:function(clave)
					{
						if (clave==1){
							$('#formu').submit();
						}else
						{
							alert('Contraseña Incorrecta');
							$('#clave').val('');
						}
					}
				});
			}
		}
		function mayus(){
			var nombre=$("#usuario").val();

			$("#usuario").val(nombre.toLowerCase());
		}
		function validar_mail()
		{
			var correo=$('#correo').val();
			var url = '<?php echo base_url(); ?>Login_controller/validar_mail';
			$.ajax({
				url:url,
				data:'correo='+correo,
				type:'post',
				success: function(val){
					if(val==0){
						alert('Correo No Registrado');
						
					}else{
						alert('Codigo enviado con exito');
						$('#formu2').submit();
					}
				}
			});
		}
	</script>
	<script>
		var input = document.getElementById("clave");
		input.addEventListener("keyup", function(event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("button").click();
			}
		});
	</script>
	<script>
		var correoo = document.getElementById("correo");
		correoo.addEventListener("keyup", function(event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("enviar").click();
			}
		});
	</script>
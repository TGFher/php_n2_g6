<!DOCTYPE html>
<html>
<head>
	<title>Mostrar Vehiculos</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link href="<?php echo base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap.min.js">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
</head>
<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg'); color: white; font-family: comic sans ms; background-attachment: fixed;">
	<br>
	<div class="container-fluid">
		<div>
			<div class="offset-5 my-5">
				<font size="+5">Vehiculos</font>
			</div>
			<div class="container">
				<form>
					<div class="row">
						<div class="col-md-4">
							<input id="buscar" class="form-control" type="text" placeholder="Search">
						</div>
						<div class="offset-4 col-md-4">
							<a type="button" href="<?php echo base_url();?>car_controller/marca" class="btn btn-success">Agregar Vehiculo</a>
						</div>
					</div>
				</form>
			</div>
			<br>
		</div>
		<div class="table-wrapper-scroll-y">
			<table class="table table-dark table-hover" id="tabla">
				<thead>
					<th hidden>Id</th>
					<th>Marca</th>
					<th>Año</th>
					<th>Placa</th>
					<th>Modelo</th>
					<th>Color</th>
					<th>Tipo</th>
					<th>Alquiler/dia</th>
					<th>Fianza</th>
					<th>Estado</th>
					<th>Aire</th>
					<th>Combustible</th>
					<th>Transmisión</th>
					<th>Capacidad</th>
					<td></td>
					<th>Actualizar</th>
					<th>Eliminar</th>
				</thead>
				<?php 
				foreach ($most as $c) { ?>
					<tr>
						<td hidden> <?php echo $c->Id_vehiculo ?></td>
						<td><?php echo $c->Marca ?></td>
						<td><?php echo $c->Anio ?></td>
						<td><?php echo $c->Placa ?></td>
						<td><?php echo $c->Modelo ?></td>
						<td><?php echo $c->Color ?></td>
						<td><?php echo $c->TIpo_auto ?></td>
						<td><?php echo '$'.$c->Precio_dia ?></td>
						<td><?php echo '$'.$c->Fianza ?></td>
						<td><?php echo $c->Estado_vehiculo ?></td>
						<td><?php echo $c->aire ?></td>
						<td><?php echo $c->combustible ?></td>
						<td><?php echo $c->transmision ?></td>
						<td><?php echo $c->capacidad ?></td>
						<td><img src="<?php echo base_url()?>assets/img/<?=$c->url_imagen?>" width="35px" heigth="35px" class="card-signin26"></td>
						<td><a href="<?php echo base_url()?>car_controller/view_actuali?idcar=<?=$c->Id_vehiculo?>" class="btn btn-info btn-round">Actualizar</a></td>
						<td><a href="<?php echo base_url()?>car_controller/elimi_car?idcar=<?=$c->Id_vehiculo?>" class="btn btn-danger btn-round" onclick=" return confirm('¿Estas seguro que quieres eliminar el registro del auto?');">Eliminar</a></td>
					</tr>
				<?php } ?>
			</table>
		</div>
	</div>
</body>
</html>
<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>
<script language="javascript">
	var busqueda = document.getElementById('buscar');
	var table = document.getElementById("tabla").tBodies[0];

	buscaTabla = function(){
		texto = busqueda.value.toLowerCase();
		var r=0;
		while(row = table.rows[r++])
		{
			if ( row.innerText.toLowerCase().indexOf(texto) !== -1 )
				row.style.display = null;
			else
				row.style.display = 'none';
		}
	}
	busqueda.addEventListener('keyup', buscaTabla);
</script>
<script type="text/javascript">
	$('th').click(function() {
		var table = $(this).parents('table').eq(0)
		var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
		this.asc = !this.asc
		if (!this.asc) {
			rows = rows.reverse()
		}
		for (var i = 0; i < rows.length; i++) {
			table.append(rows[i])
		}
		setIcon($(this), this.asc);
	})

	function comparer(index) {
		return function(a, b) {
			var valA = getCellValue(a, index),
			valB = getCellValue(b, index)
			return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
		}
	}

	function getCellValue(row, index) {
		return $(row).children('td').eq(index).html()
	}

	function setIcon(element, asc) {
		$("th").each(function(index) {
			$(this).removeClass("sorting");
			$(this).removeClass("asc");
			$(this).removeClass("desc");
		});
		element.addClass("sorting");
		if (asc) element.addClass("asc");
		else element.addClass("desc");
	}
</script>
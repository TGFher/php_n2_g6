<!DOCTYPE html>
<html>
<head>
  <title>Registro de Renta</title>
  <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>
  <link href="<?php echo base_url()?>assets/css/demo.css" rel="stylesheet" />
 
</head>
<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg'); color: white; font-family: comic sans ms; background-attachment: fixed;">
  <br>
  <div class="container">
    
      <div class="offset-5 my-5 mx-auto">
        <center><font size="+5">Registro de Renta</font></center>
      </d
      iv>
      <table class="table  table-dark table-hover text-center">
       <tr>
        <th hidden>Id</th>
        <th><font color="white">DUI</font></th>
        <th><font color="white">Vehiculo</font></th>
        <th><font color="white">Fecha de reserva</font></th>
        <th><font color="white">Fecha de devolucion</font></th>
        <th><font color="white">Hora de reserva</font></th>
        <th><font color="white">Hora de devolucion</font></th>
        <th><font color="white">Anexo</font></th>
        <th><font color="white">Fianza</font></th>
        <th><font color="white">Costo por dia</font></th>
        <th><font color="white">Horas</font></th>
        <th><font color="white">Dias</font></th>
        <th><font color="white">Costo del alquiler</font></th>
        <th><font color="white">Actualizar</font></th>
        <th><font color="white">Eliminar</font></th>
    </tr>

    <?php
    foreach ($usuario as $f){ ?>
      <tr>

       <td><?php echo $f->DUI_id ?></td>
       <td><?php echo $f->Modelo ?></td>
       <td><?php echo $f->Fecha_reserva ?></td>      
       <td><?php echo $f->Fecha_devolucion ?></td>
       <td><?php echo $f->Hora_reserva ?></td>
       <td><?php echo $f->Hora_devolucion ?></td>
       <td><?php echo $f->Anexo ?></td>
       <td><?php echo $f->Fianza ?></td>
      <td><?php echo $f->costo_alquiler ?></td>
       <td><?php echo $f->Hora ?></td>
       <td><?php echo $f->Dia ?></td>
        <td><?php echo $f->Costo_total ?></td>

          <td><a href="<?php echo base_url()?>renta_controller/llenar_renta?id=<?=$f->Id_renta?>" class="btn btn-success">Actualizar</a></td> 

         <td><a href="<?php echo base_url()?>renta_controller/eliminar_renta?id=<?=$f->Id_renta?>"
          class="btn btn-danger" onclick=" return confirm('Estas seguro que quieres eliminar el registro del usuario?');">Eliminar</a></td> 

            <?php } ?>
               </tr>

     
 
 </table>
 <br>
</div>
</body>
</html>


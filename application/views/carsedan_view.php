<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link href="<?php echo base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap.min.js">
</head>
<body style="color: white; font-family: comic sans ms" class="bodyza">
	<br>

	<div class="container my-4">
		<br>
		<div class="row"> 	
			<div class="col-md-4 my-5"><input placeholder="Buscar" class="form-control" type="" name="" ></div>
			<div class="col-md-4 my-5 offset-4">
				<a href="<?php echo base_url()?>car_controller/carrito"><input class="btn btn-secondary"  value="Regresar"></a>
			</div>
		</div>
		<div class="row my-4">
			<?php foreach ($ver as $v) {?>
				<div class="col-md-4">
					<div class="container">
						<img src="<?php echo base_url();?>assets/img/<?=$v->url_imagen?>" width="250px" heigth="250px" class="card-signin25">
						<div style="height: 60px"></div>
						<div class="texto-encima1">
							<div class="modal fade" id="<?=$v->Id_vehiculo?>" tabindex="-1" role="dialog" aria-labelledby="<?=$v->TIpo_auto?>" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="<?=$v->Id_vehiculo?>"><b>Caracteristicas</b></h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body" id="<?=$v->Id_vehiculo?>">
											<form id="formu" method="POST" >
												<table class="table">
													<tbody id="prueba<?=$v->Id_vehiculo?>">

													</tbody>		
												</table>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
												<a href="<?php echo base_url()?>renta_controller/vehiculito?idcar=<?=$v->Id_vehiculo ?>"><button type="button" class="btn btn-primary">Rentar</button></a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="texto-encima1"><button type="button"   onclick="tipo_ver('<?=$v->Id_vehiculo?>','<?=$v->TIpo_auto?>','<?=$v->Estado_vehiculo?>')"  class="btn btn-info" data-toggle="modal" data-target="#<?=$v->Id_vehiculo?>">Especificaciones</button>
							</div>
						</form>

					</div>
				</div>
			<?php } ?>
		</div>
	</div>
	<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
</body>
</html>
<script type="text/javascript">
	function tipo_ver(id,tipo,estado)
	{
		$('#prueba').html('');
		var url='<?php echo base_url();?>car_controller/info_car';
		$.ajax({
			url: url,
			data:'id='+id+'&tipo='+tipo,
			type: 'get',
			success:function(hola)
			{	
				$('#prueba'+id).html(hola);
				$('#button').reset.onclick();
			}
		});
	}
</script>
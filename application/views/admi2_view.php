<!DOCTYPE html>
<html>
<head>
	<title>Administrador Registrado</title>
	 <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/js/bootstrap.min.js">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
	 </head>
<body>
	<div class="container">
		<div>
			<center><h3>ADMINISTRADOR REGISTRADO</h3></center>
		</div>
		<table>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Edad</th>
				<th>Fecha Nacimiento</th>
				<th>Dui</th>
				<th>Sexo</th>
				<th>Puesto</th>
				<th>Telefono</th>
				<th>Telefono Emergencia</th>
			</tr>
			<?php foreach ($this->model->listar_admi() as $l): ?>
			<tr>
				<td><?php echo $l->Id_empleados ?></td>
				<td><?php echo $l->Nombre ?></td>
				<td><?php echo $l->apellido ?></td>
				<td><?php echo $l->Edad ?></td>
				<td><?php echo $l->Fnacimiento ?></td>
				<td><?php echo $l->DUI ?></td>
				<td><?php echo $l->Sexo_id ?></td>
				<td><?php echo $l->Puesto_id ?></td>
				<td><?php echo $l->Telefono ?></td>
				<td><?php echo $l->Telefono_emergencia ?></td>
                
			</tr>
		<?php endforeach; ?>
		</table>
		<br>
		<center><a href="?r=usuario" class="btn indigo lighten-3">REGRESAR</a></center>
		
		
	</div>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Usuario</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/js/bootstrap.min.js">
	<link href="<?php echo base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/css/demo.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style.css">
</head>
<body style="background-image: url('<?php echo base_url()?>assets/img/Carro_bg.jpg'); color: white; font-family: comic sans ms; background-attachment: fixed;">
	<br><br><br>
	<div class="card-signin2 col-md-9 mx-auto my-5"> 
		<center><font size="+4">Nuevo Usuario</font></center>
		<div class="container"> 
			<form method="POST" action="<?php echo base_url()?>login_controller/agregar">        
				<br>
				<div class="row">
					<div class="offset-1 col-md-5"> <p style="color:#FFFFFF">Nombre del Usuario</p> 
						<input id="nombre" class="form-control" type="text" name="nombre" minlength="5" maxlength="60" pattern="[a-Z]" placeholder="Nombre Usuario" title="Solo letras y minimo de 5" required onkeyup="convertir(2)">
					</div>                         
					<br>
					<div class="col-md-5">
						<p style="color:#FFFFFF">Usuario</p>
						<input id="usuario" class="form-control" type="text" name="usuario" placeholder="Usuario" minlength="5" maxlength="15"  title="Min de 5 y Max de 15" onkeyup="minus()" onblur="validar_user()" required onkeypress="if(event.keyCode==32)event.returnValue=false;">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="offset-1 col-md-5">
						Contraseña
						<input class="form-control" type="password" id="clave1" name="clave" placeholder="Clave" required onkeypress="if(event.keyCode==32)event.returnValue=false;">
						<input type="checkbox" id="mostrar_clave" title="Clic para mostrar clave">
					</div>
					<div class="col-md-5">
						<p style="color:#FFFFFF">Contraseña</p>
						<input class="form-control" type="password" id="clave2" name="registro" placeholder="Clave" required onkeypress="if(event.keyCode==32)event.returnValue=false;">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="offset-1 col-md-5">
						<p style="color:#FFFFFF">Correo Electronico</p>
						<input type="email" id="correo" name="correo" placeholder="Correo electronico" class="form-control" required onblur="validar_mail()">
					</div>
					<div class="col-md-5">
						<p style="color:#FFFFFF">Rol</p>
						<select name="rol" class="form-control" required>
							<option value="">--Rol--</option>
							<?php foreach($usuario as $u){ ?>
								<option value="<?=$u->idrol?>"><?=$u->rol ?></option>
							<?php } ?>
						</select>	
					</div>
				</div>
				<br>
				<br>
				<tr>
					<center><td><input type="submit" name="g" value="Guardar" class="btn btn-primary btn-round"></td></center>
				</tr>
			</div>      	 
	</form>
</div>
</div>
</body>
</html>
<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>
<script type="text/javascript"> 
	window.onload = function () {
		document.getElementById("clave1").onchange = validatePassword;
		document.getElementById("clave2").onchange = validatePassword;
	}
	function validatePassword(){
		var pass2=document.getElementById("clave2").value;
		var pass1=document.getElementById("clave1").value;
		if(pass1!=pass2)
			document.getElementById("clave1").setCustomValidity("Las contraseñas no coinciden");
		else
			document.getElementById("clave1").setCustomValidity('');
	}
	function minus(){
		var nombre=$("#usuario").val();

		$("#usuario").val(nombre.toLowerCase());
	}
	function validar_user()
	{
		var user=$('#usuario').val();
		var url = '<?php echo base_url(); ?>Login_controller/validar_user';
		$.ajax({
			url:url,
			data:'user='+user,
			type:'post',
			success: function(data){
				if(data==1){
					alert('Usuario Ya Registrado');
				}else{
					$('#usuario').addClass('good');

				}
			}
		});
	}
	function validar_mail()
	{
		var correo=$('#correo').val();
		var url = '<?php echo base_url(); ?>Login_controller/validar_mail';
		$.ajax({
			url:url,
			data:'correo='+correo,
			type:'post',
			success: function(val){
				if(val==1){
					alert('Correo Ya Registrado');
				}else{
					$('#usuario').addClass('good');

				}
			}
		});
	}
	function convertir(tipo)
	{
		var nombre = document.getElementById("nombre");
		var texto = nombre.value;
		if (tipo == 2){
			nombre.value = texto.toLowerCase();
			nombre.value = nombre.value.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g,
				function($1)
				{
					return $1.toUpperCase(); 
				});
		}
	}
	$(document).ready(function () {
        $('#mostrar_clave').click(function () {
          if ($('#mostrar_clave').is(':checked')) {
            $('#clave1').attr('type', 'text');
          } else {
            $('#clave1').attr('type', 'password');
          }
        });
      });
</script>
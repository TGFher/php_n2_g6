<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body style="color: white">
	<br>
	<div class="container">
		<form action="<?php echo base_url();?>login_controller/actualizar_clave" method="post">
			<br><br><br><br>
			<div class="card col-md-6 mx-auto my-5">
				<div class="my-3">
				<center><h2>Actualizar Clave</h2></center>
				</div>
				<div class="col-md-12">
					<input type="hidden" name="idusuario" value="<?=$usuario->idusuario?>">
					<input type="hidden" name="recu" value="<?=$usuario->recu?>">
					Usuario:
					<input type="text" name="user" value="<?=$usuario->usuario?>" readonly class="form-control">
					Clave:
					<input type="password" id="clave1" name="clave" value="<?=$usuario->registro?>" class="form-control" onkeypress="if(event.keyCode==32)event.returnValue=false;">
					Confirmar Clave:
					<input type="password" id="clave2" name="registro" value="<?=$usuario->registro?>" class="form-control" onkeypress="if(event.keyCode==32)event.returnValue=false;">
					<br><br>
					<center><button type="submit" class="btn btn-success">Guardar</button></center>
				</div>
			</div>
		</form>
	</div>
</body>
</html>
<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>
<script type="text/javascript"> 
	window.onload = function () {
		document.getElementById("clave1").onchange = validatePassword;
		document.getElementById("clave2").onchange = validatePassword;
	}
	function validatePassword(){
		var pass2=document.getElementById("clave2").value;
		var pass1=document.getElementById("clave1").value;
		if(pass1!=pass2)
			document.getElementById("clave1").setCustomValidity("Las contraseñas no coinciden");
		else
			document.getElementById("clave1").setCustomValidity('');
	}
</script>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class renta_controller extends CI_Controller{

	public function __construct(){
		parent:: __construct();
		$this->load->model('renta_model');
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function index(){
		$this->load->view('renta_view');
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public function  wacha(){
		   if($this->session->userdata('usuario')==''){
		redirect('login_controller/index');
    }	
  	$tip=$this->renta_model->select_dui();
  	$mar['dui']= $tip;

  	$tip=$this->renta_model->select_vehiculo();
  	$mar['vehiculo']= $tip;

  	$tip=$this->renta_model->select_anexo();
  	$mar['anexo']= $tip;

  	$this->load->view('navbar');
  	$this->load->view('renta_view',$mar);


	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	public function insertar_renta(){
	$data['DUI_id']=$_POST["dui"];
  	$data['Vehiculo_id']=$_POST["vehiculo"];
  	$data['Fecha_reserva']=$_POST["f_reserva"];
  	$data['Fecha_devolucion']=$_POST["f_devolucion"];
  	$data['Hora_reserva']=$_POST["h_reserva"]; 
  	$data['Hora_devolucion']=$_POST["h_devolucion"];
  	$data['Anexo_id']=$_POST["anexo"];
  	$data['Fianza']=$_POST["fianza"];
  	$data['Costo_total']=$_POST["cdia"];
  	$data['Dia']=$_POST["dia"];
  	$data['Hora']=$_POST["hora"];
  	$data['costo_alquiler']=$_POST["costo_alquiler"];
  	 $this->renta_model->insertar_renta($data);
	redirect('/renta_controller/mostrar_renta');

	$this->renta_controller->cambiar_estado();
	}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	public function mostrar_renta(){
		   if($this->session->userdata('usuario')==''){
		redirect('login_controller/index');
    }
	$this->load->view('navbar');
  	$ver= $this->renta_model->mostrar_renta();
 	$ren["usuario"]= $ver;
  	$this->load->view('mosrenta_view',$ren);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function eliminar_renta(){
		$id = $_REQUEST['id'];
		$this->renta_model->eliminar_renta($id);
		redirect('renta_controller/mostrar_renta');
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function llenar_renta(){
		   if($this->session->userdata('usuario')==''){
	redirect('login_controller/index');
    }
		$con = $this->renta_model->select_dui();
		$data['dui'] = $con;

		$asd = $this->renta_model->select_vehiculo();
		$data['vehiculo'] = $asd;

		$c = $this->renta_model->select_anexo();
		$data['anexo'] = $c;

		$data['renta'] = $this->renta_model->llenar_r($_REQUEST['id']);
		$this->load->view('navbar');
		$this->load->view('reactualizar_view', $data);

	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function actualizar_renta(){


		$data ['id'] = $_POST['id'];
		$data ['DUI_id'] = $_POST['dui'];
		$data ['Vehiculo_id'] = $_POST['vehiculo'];
		$data['Fecha_reserva'] = $_POST['f_reserva'];
		$data['Fecha_devolucion'] = $_POST['f_devolucion'];
		$data['Hora_reserva']=$_POST["h_reserva"];
  		$data['Hora_devolucion']=$_POST["h_devolucion"]; 
		$data['Anexo_id'] = $_POST['anexo'];
		$data['Fianza'] = $_POST['fianza'];
		$data['Costo_total']=$_POST["cdia"];
  		$data['Dia']=$_POST["dia"];
  		$data['Hora']=$_POST["hora"];
		$data['costo_alquiler'] = $_POST['costo_alquiler'];
		$this->renta_model->actualizar_r($data);
		redirect('renta_controller/mostrar_renta');
}


public function llenar_nombre(){

echo $this->renta_model->mostrar_nombre($_POST['Id_DUI']);
}

	
	public function vehiculito(){

				   if($this->session->userdata('usuario')==''){
		redirect('login_controller/index');
    }

		$con = $this->renta_model->select_dui();
		$data['dui'] = $con;

				$c = $this->renta_model->select_anexo();
		$data['anexo'] = $c;


		$data['vehiculo'] =$this->renta_model->recu($_REQUEST['idcar']);
		$this->load->view('navbar');
		$this->load->view('renta_view', $data);

		

	}

	public function cambiar_estado(){
		$estado = $this->renta_model->select_anexo();
		$data['anexo'] = $c;

		$data['renta'] = $this->renta_model->llenar_r($_REQUEST['id']);
		$this->load->view('navbar');
		$this->load->view('reactualizar_view', $data);



	}

}


 ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_empleado extends CI_Controller {

	public function __construct() 
	{
		parent:: __construct();
		$this->load->model('usuario_model');
	}

	public  function agregar_dato()
	{
		//carga la vista donde estara el formulario para agregar un nuevo empleado
		$this->load->view('agregar_usuario_view');
	}

	public function agregar()
	{
		$data['nombre'] = $_POST['nombre'];
		$data['apellido'] = $_POST['apellido'];
		$data['edad'] = $_POST['edad'];
		$data['Fnacimiento'] = $_POST['Fnacimiento'];
		$data['DUI'] = $_POST['DUI'];
		$data['sexo'] = $_POST['sexo'];
		$data['puesto'] = $_POST['puesto'];
		$data['telefono'] = $_POST['telefono'];
		$data['tel_emergencia'] = $_POST['tel_emergencia'];
		$data['direccion'] = $_POST['direccion'];
		$this->usuario_model->insertar($data);
		$this->mostrar_view();

	}

	public function mostrar()
	{
		$usuario = $this->usuario_model->primario();
		$data['usuario'] = $usuario;
		$this->load->view('mostrar_view', $data);
	}


	public function eliminar_a()
	{
		$this->load->view('eliminar');
	}
	public function eliminar()
	{
		$dui = $_POST['dui'];
		$this->usuario_model->eliminar($dui);
		$this->index();
	}

	public function accion(){

		$data['usuario'] = $this->usuario_model->obtener($_POST['editar']);
		$this->load->view('editar', $data);
	}

	public function editar(){

		$data['nombre'] = $_POST['nombre'];
		$data['apellido'] = $_POST['apellido'];
		$data['edad'] = $_POST['edad'];
		$data['Fnacimiento'] = $_POST['Fnacimiento'];
		$data['DUI'] = $_POST['DUI'];
		$data['sexo'] = $_POST['sexo'];
		$data['puesto'] = $_POST['puesto'];
		$data['telefono'] = $_POST['telefono'];
		$data['tel_emergencia'] = $_POST['tel_emergencia'];
		$data['direccion'] = $_POST['direccion'];
		$this->empleado_model->actualizar($data);
		$this->mostrar_view();

	}

}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Login_model');
	}
	public function index()
	{
		if($this->session->userdata('usuario')!='')
		{	
			if($this->session->userdata('rol_id')==1)
			{
				$this->load->view('navbar');
				$this->load->view('inicio');
			}elseif($this->session->userdata('rol_id')==2)
			{
				$this->load->view('navbar');	
				$this->load->view('inicio');
			}else{
				$this->load->view('login_view');
			}
		}else
		{
			$this->load->view('login_view');
		}
	}
	public function validar_user()
	{
		$user=$this->input->post('user');
		echo $this->Login_model->validar_user($user);
	}
	public function validar_clave()
	{
		echo $this->Login_model->validar_clave($_POST['usuario'],sha1(md5($_POST['clave'])));
	}
	public function validar_codigos()
	{
		echo $this->Login_model->validar_codigos($_POST['codigo']);
	}
	public function validar_mail()
	{
		$mail=$this->input->post('correo');
		echo $this->Login_model->validar_mail($mail);
	}
	public function validar()
	{
		if($_POST["clave"]==NULL){
			redirect('');
		}
		if($consul=$this->Login_model->Validar_usuario($_POST['usuario'], sha1(md5($_POST['clave']))))
		{
			foreach ($consul as $r) {
				$rol_id=$r->rol_id;
				$nombre_usuario=$r->nombre_usuario;
				$usuario=$r->usuario;
				$clave=$r->clave;
			}
			$arreglo=array('usuario'=>$usuario, 'nombre_usuario'=>$nombre_usuario, 'rol_id'=>$rol_id, 'clave'=>$clave);
			$this->session->set_userdata($arreglo);
			if($rol_id==1)
			{
				$_POST['clave']=NULL;
				redirect('');
			}else{
				$_POST['clave']=NULL;
				redirect('');
			}
		}
		else
		{
			?>
			<?php
		}
		$this->load->view('login_view');
	}
	public function recu_clave()
	{
		$this->load->view('recu_clave');
	}
	public function recuperar_clave()
	{
		$correo = $this->input->post('correo');
		if($consul = $this->Login_model->enviar_clave($correo))
		{
			foreach ($consul as $l) {
				$nombre=$l->nombre_usuario;
				$correo=$l->correo;
				$clave=$l->recu;
			}
			$para = $correo;
			$asunto = "Recuperacion de contraseña";
			$mensaje = "Su código de recuperación de clave es $clave
			Ingrese al siguiente enlace: 
			http://localhost/PHP_N2_G6/Login_controller/codigo";
			$encabezado = "From: zahir.menjivar@gmail.com";

			if(isset($correo)) {
				mail($para, $asunto, $mensaje, $encabezado);
				
				$this->load->view('login_view');
			}
			else{
				echo "Error en el envío.";
			}
		}
		else
		{
			?>
			<?php
			$this->load->view('login_view');
		}
	}
	public function codigo()
	{
		$this->load->view('codigo');
	}
	public function validar_codigo()
	{
		$codigo=$this->input->post('codigo');
		if($usuario=$this->Login_model->validar_codigo($codigo)){
			$data['usuario'] = $usuario;
			$this->load->view('actualizar_clave', $data);
		}else{
			?>
			
			<?php
			
		}
	}
	public function actualizar_clave()
	{
		$token = bin2hex(random_bytes((6 - (6 % 2)) / 2));
		$data['idusuario'] =$_POST['idusuario'];
		$data['clave'] = sha1(md5($_POST['clave']));
		$data['recu'] = $token;
		$this->Login_model->actualizar_clave($data);
		redirect('');
	}
	public function mostrarrol()
	{
		$usuario = $this->Login_model->llenarrol();
		$data['usuario'] = $usuario;
		$this->load->view('mostrar_view', $data);
	}
	public function mostrar_usuario()
	{
		if($this->session->userdata('usuario')==""){
			redirect('');
		}
		$this->load->view('navbar');
		$usuario = $this->Login_model->mostrar_usuario();
		$data['usuario'] = $usuario;
		$this->load->view('usuario2_view', $data);
	}
	public function barra_admin(){
		if($this->session->userdata('usuario')=='')
		{
			redirect('');
		}
		$usuario = $this->Login_model->llenarrol();
		$data['usuario'] = $usuario;
		$this->load->view('navbar');
		$this->load->view('usuario_view',$data);
	}
	public function agregar()
	{
		if($this->session->userdata('usuario')=='')
		{
			redirect('');
		}
		$token = bin2hex(random_bytes((6 - (6 % 2)) / 2));
		$this->load->view('navbar');
		$data['nombre'] = htmlspecialchars($_POST['nombre']);
		$data['usuario'] = $_POST['usuario'];
		$data['clave'] = $_POST['clave'];
		$data['registro']=$_POST['registro'];
		$data['recu'] = $token;
		$data['rol'] = $_POST['rol'];
		$data['correo'] = $_POST['correo'];
		$this->Login_model->insertar_usuario($data);
		redirect('Login_controller/mostrar_usuario');
	}
	public function eliminar()
	{
		$id = $_REQUEST['id'];
		$this->Login_model->eliminar_usuario($id);
		redirect('Login_controller/mostrar_usuario');
	}
	public function accion_actualizar(){
		if($this->session->userdata('usuario')==''){
			redirect('');
		}
		$rol = $this->Login_model->mostrar_rol();
		$data["rol"] = $rol;
		$data['usuario'] = $this->Login_model->Llenar_usuario($_REQUEST['id']);
		$this->load->view('navbar');
		$this->load->view('editar', $data);
	}
	public function actualizar(){
		$token = bin2hex(random_bytes((6 - (6 % 2)) / 2));
		$data ['id'] = $_POST['id'];
		$data ['nombre'] = $_POST['nombre'];
		$data ['usuario'] = $_POST['usuario'];
		$data['clave'] = sha1(md5($_POST['clave']));
		$data['recu'] = $token;
		$data['rol'] = $_POST['rol'];
		$data['correo'] = $_POST['correo'];
		$data['registro'] = $_POST['registro'];
		$this->Login_model->actualizar_usuario($data);
		redirect('Login_controller/mostrar_usuario');
	}
	public function cerrar_sesion()
	{
		$this->session->sess_destroy();
		redirect('');
	}
}
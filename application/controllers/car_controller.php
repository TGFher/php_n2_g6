<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_controller extends CI_Controller {
	public function __construct(){ 
   parent:: __construct();
   $this->load->model('car_model');

 }

 public function index(){
  if($this->session->userdata('usuario')==''){
    redirect('login_controller/index');
  }
  $this->load->view('');


}


public function insertar_auto(){

 $ins['Marca_id']=$_POST["marca"];
 $ins['Anio']=$_POST["anio"];
 $ins['Placa']=$_POST["placa"];
 $ins['Modelo']=$_POST["modelo"];
 $ins['Color']=$_POST["color"];
 $ins['Tipo_auto_id']=$_POST["tipo_auto"];
 $ins['Precio_dia']=$_POST["precio_dia"];
 $ins['Estado_id']=$_POST["estado_auto"];
 $ins['caracteristica_id']=$_POST["caracteris"];
 $ins['foto']=$_POST["foto"];
 $ins['Fianza']=$_POST["fianza"];
 

 $this->car_model->inser_aut($ins);
 redirect('/car_controller/most_car');
}
public function marca(){
  if($this->session->userdata('usuario')==''){
    redirect('login_controller/index');
  }
  $var= $this->car_model->selc_marca();
  $mar['mar']= $var;

  $tip=$this->car_model->tipo_car();
  $mar['tipo']= $tip;

  $tip=$this->car_model->tipo_estado();
  $mar['estado']= $tip;

  $tip=$this->car_model->tipo_caract();
  $mar['caract']= $tip;

  $this->load->view('navbar');
  $this->load->view('carinser_view',$mar);

}

public function most_car(){
 if($this->session->userdata('usuario')==''){
  redirect('login_controller/index');
}

$ver= $this->car_model->mostrar_car();
$aut["most"]= $ver;
$this->load->view('navbar');
$this->load->view('car_mostrar' ,$aut);
}

public function elimi_car(){
  $id= $_REQUEST["idcar"];
  $this->car_model->eliminar_carro($id);
  redirect('/car_controller/most_car');
}
public function view_actuali(){
 if($this->session->userdata('usuario')==''){
  redirect('login_controller/index');
}
$id =$this->car_model->mostar_carro($_REQUEST["idcar"]);
$data["car"] = $id;
$marca= $this->car_model->mostrar_marca();
$data["marca"]= $marca;
$est=$this->car_model->mostr_est();
$data['est']=$est;
$tipo=$this->car_model->mostra_tipo();
$data['tip']=$tipo;
$cat=$this->car_model->mostrar_caracter();
$data['cart']=$cat;
$foto=$this->car_model->fto();
$data['fto']=$foto;
$this->load->view('navbar');
$this->load->view('carupdate_view',$data);
}

public function actuali_car(){

  $ins['id']=$_POST["id_car"];
  $ins['Marca_id']=$_POST["marca"];
  $ins['Anio']=$_POST["anio"];
  $ins['Placa']=$_POST["placa"];
  $ins['Modelo']=$_POST["modelo"];
  $ins['Color']=$_POST["color"];
  $ins['Tipo_auto_id']=$_POST["tipo_auto"];
  $ins['Precio_dia']=$_POST["precio_dia"];
  $ins['Estado_id']=$_POST["estado_auto"];
  $ins['caracteristica_id']=$_POST["caracteris"];
  $ins['foto']=$_POST["foto"]; 
  $ins['Fianza']=$_POST["fianza"];   
  $this->car_model->actualizar_car($ins);
  redirect('/car_controller/most_car');
}

public function carrito(){
  if($this->session->userdata('usuario')==''){
    redirect('login_controller/index');
  }
  $this->load->view('navbar');
  $this->load->view('carselec_view');
}
public function carritos2(){
  if($this->session->userdata('usuario')==''){
    redirect('login_controller/index');
  }

  $ver= $this->car_model->mostrar_car2($_REQUEST['id']);
  $dato["ver"]= $ver;
  $this->load->view('navbar');
  $this->load->view('carsedan_view',$dato);
}


public function info_car()
{
 $datos= $this->car_model->mostrar_car_by_id($_GET['id'], $_GET['tipo']);

 foreach ($datos as $x) {
  echo "<tr>";
  echo '<td hidden>'.$x->Id_vehiculo.'</td>';
  echo '<td>'.'Marca:     '.''.$x->Marca.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Año:     '.$x->Anio.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Placa:   '.$x->Placa.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Modelo:   '.$x->Modelo.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Color:    '.$x->Color.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Tipo:     '.$x->TIpo_auto.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Arrendamiento/dia:     $' .$x->Precio_dia.'</td>';
  echo '</tr>';
  echo '<td>'.'Fianza:       $' .$x->Fianza;
  echo '</tr>';
  echo '<tr>';
  echo '<td hidden>'.$x->Estado_vehiculo.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Aire acondicionado:   '.$x->aire.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Combustible:       ' .$x->combustible. '</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td>'.'Transmision:    ' .$x->transmision. '</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<tr>'.'Capacidad:      ' .$x->capacidad. '</td>';

  echo '</tr>';
}

}

public function stock_carr(){
 if($this->session->userdata('usuario')==''){
    redirect('login_controller/index');
  }

$ver= $this->car_model->stock_car();
  $dato['most']= $ver;
  $this->load->view('navbar');
  $this->load->view('stock_view',$dato);
}


public function ingremarc(){

$dato=$_POST["marca"];

$this->car_model->ingremar($dato);
redirect('car_controller/marca');


}
 
 public function mttomarca(){
$mtt['vid']=$this->car_model->mmtto();
 $this->load->view('navbar');
 $this->load->view('mmtto_view',$mtt);

 }

 public function elimar(){
 $el=$_REQUEST["idm"];
 $this->car_model->elimimar($el);
redirect('car_controller/marca');
 }


 public function actum(){
$id['id']=$_REQUEST["idm"];
$id['mar']=$_POST["marca1"];
 $this->car_model->actualim($id);
redirect('car_controller/marca');

 }






}



?>
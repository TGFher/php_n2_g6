
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Control extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('vehi_model');
	}

public function index()
	{
		if($this->session->userdata('usuario')==""){
			redirect('');
		}
		$this->load->view('navbar');
		$usuario = $this->vehi_model->mostrar_vehi();
		$data['usuario'] = $usuario;
		$this->load->view('mosve', $data);
	}
}

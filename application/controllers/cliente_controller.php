<?php 
defined('BASEPATH') or exit ('No direct script access allowed');

class cliente_controller extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('cliente_model');
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function index(){
		if ($this->session->userdata('usuario')==''){
			redirect('login_controller/index');
		}
		$we=$this->cliente_model->select_documento();
		$wen['documento']=$we;

		$we=$this->cliente_model->select_pais();
		$wen['pais']=$we;

		$we=$this->cliente_model->select_tipo();
		$wen['tipo']=$we;

		$we=$this->cliente_model->select_sexo();
		$wen['sexo']=$we;

		$this->load->view('navbar');
		$this->load->view('cliente_view', $wen);
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function insertar_cliente(){
		$pao['Id_DUI']=$this->input->post('Id_DUI');
		$pao['Nombre']=$this->input->post('Nombre');
		$pao['Apellido']=$this->input->post('Apellido');
		$pao['Edad']=$this->input->post('Edad');
		$pao['Fnacimiento']=$this->input->post('Fnacimiento');
		$pao['Sexo_id']=$this->input->post('Sexo_id');
		$pao['Tipo_licencia_id']=$this->input->post('Tipo_licencia_id');
		$pao['Documento_id']=$this->input->post('Documento_id');
		$pao['Numero_licencia']=$this->input->post('Numero_licencia');
		$pao['Pais_id']=$this->input->post('Pais_id');
		$pao['Telefono']=$this->input->post('Telefono');
		$pao['Telefono_referencia']=$this->input->post('Telefono_referencia');
		
		$this->cliente_model->insertar_cliente($pao);
		redirect('cliente_controller/mostrar_cliente');
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function mostrar_cliente(){
		if ($this->session->userdata('usuario')==''){
			redirect('login_controller/index');
		}
		$this->load->view('navbar');
		$ver=$this->cliente_controller->mostrar_cliente();
		$cli['cliente']=$ver;
		$this->load->view('moscliente_view', $cli);
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function llenar_cliente(){
		$we=$this->cliente_model->select_documento();
		$wen['documento']=$we;

		$we=$this->cliente_model->select_pais();
		$wen['pais']=$we;

		$we=$this->cliente_model->select_tipo();
		$wen['tipo']=$we;

		$we=$this->cliente_model->select_sexo();
		$wen['sexo']=$we;

		$wen['cliente']=$this->cliente_model->llenar_cliente($_REQUEST['id']);

		$this->load->view('navbar');
		$this->load->view('cliente_view', $wen);

	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function actualizar_cliente(){
		$pao['id']=$this->input->post('id');
		$pao['Id_DUI']=$this->input->post('Id_DUI');
		$pao['Nombre']=$this->input->post('Nombre');
		$pao['Apellido']=$this->input->post('Apellido');
		$pao['Edad']=$this->input->post('Edad');
		$pao['Fnacimiento']=$this->input->post('Fnacimiento');
		$pao['Sexo_id']=$this->input->post('Sexo_id');
		$pao['Tipo_licencia_id']=$this->input->post('Tipo_licencia_id');
		$pao['Documento_id']=$this->input->post('Documento_id');
		$pao['Numero_licencia']=$this->input->post('Numero_licencia');
		$pao['Pais_id']=$this->input->post('Pais_id');
		$pao['Telefono']=$this->input->post('Telefono');
		$pao['Telefono_referencia']=$this->input->post('Telefono_referencia');
		$pao['Categoria_cliente_id']=$this->input->post('1');
		$this->cliente_model->insertar_cliente($pao);
		redirect('cliente_controller/mostrar_cliente');
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function eliminar_cliente(){
		$id=$_REQUEST['id'];
		$this->cliente_model->eliminar_cliente($id);
		redirect('cliente_controller7mostrar_cliente');
	}
}
 ?>
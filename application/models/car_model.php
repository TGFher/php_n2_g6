<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

 class car_model extends CI_model{

 	public function selc_marca(){
 		$consul=$this->db->get('marca');
 		return $consul->result();		

 	}
 	public function tipo_car(){
 		$tipo=$this->db->get('tipo_auto');
 		return $tipo->result();

 	}
 	public function tipo_estado(){
 		$estado=$this->db->get('estado_vehiculo');
 		return $estado->result();
 	}

 	public function tipo_caract(){
 		$caracter=$this->db->get('caracteristica');
 		return $caracter->result();
 	}

 	public function inser_aut($inser){
 		$this->db->set('Marca_id',$inser['Marca_id']);
 		$this->db->set('Anio',$inser['Anio']);
 		$this->db->set('Placa',$inser['Placa']);
 		$this->db->set('Modelo',$inser['Modelo']);
 		$this->db->set('Color',$inser['Color']);
 		$this->db->set('Tipo_auto_id',$inser['Tipo_auto_id']);
 		$this->db->set('Precio_dia',$inser['Precio_dia']);
 		$this->db->set('Estado_id',$inser['Estado_id']);
 		$this->db->set('caracteristica_id',$inser['caracteristica_id']);
 		$this->db->set('url_imagen',$inser['foto']);
 		$this->db->set('Fianza',$inser['Fianza']);
 		$this->db->insert('vehiculo');


 	}

 	public function mostrar_car(){
 		$this->db->select('v.Id_vehiculo,m.Marca,v.Anio,v.Placa,v.Modelo,v.Color,t.TIpo_auto,v.Precio_dia,v.Fianza,e.Estado_vehiculo,c.aire,c.combustible,c.transmision,c.capacidad, v.url_imagen');
 		$this->db->join('marca m','v.Marca_id=m.Id_marca','inner');
 		$this->db->join('tipo_auto t','v.Tipo_auto_id=t.Id_tipo_auto','inner');
 		$this->db->join('estado_vehiculo e','v.Estado_id=e.Id_estado_vehiculo','inner');
 		$this->db->join('caracteristica c','v.caracteristica_id=c.idcaracteristica','inner'); 		
 		$ver=$this->db->get('vehiculo v');
 		return $ver->result();
 	}
 	public function mostrar_car2($id){
 		$this->db->select('v.Id_vehiculo,m.Marca,v.Anio,v.Placa,v.Modelo,v.Color,t.TIpo_auto,v.Precio_dia,e.Estado_vehiculo,c.aire,c.combustible,c.transmision,c.capacidad, v.url_imagen');
 		$this->db->join('marca m','v.Marca_id=m.Id_marca','inner');
 		$this->db->join('tipo_auto t','v.Tipo_auto_id=t.Id_tipo_auto','inner');
 		$this->db->join('estado_vehiculo e','v.Estado_id=e.Id_estado_vehiculo','inner');
 		$this->db->join('caracteristica c','v.caracteristica_id=c.idcaracteristica','inner');
 		$this->db->where('v.Tipo_auto_id',$id);
 		 $this->db->where('e.Estado_vehiculo','Disponible');		
 		$ver=$this->db->get('vehiculo v');
 		return $ver->result();
 	}

 	public function eliminar_carro($id){
 		$this->db->where('Id_vehiculo',$id);
 		$this->db->delete('vehiculo');
 	}
 	public function mostar_carro($car){
 		$this->db->where('Id_vehiculo',$car);
 		$car = $this->db->get('vehiculo');
 		return $car->row();
 	}

 	public function actualizar_car($actual){

 		if($actual['foto']==''){
 		$this->db->set('Marca_id',$actual['Marca_id']);
 		$this->db->set('Anio',$actual['Anio']);
 		$this->db->set('Placa',$actual['Placa']);
 		$this->db->set('Modelo',$actual['Modelo']);
 		$this->db->set('Color',$actual['Color']);
 		$this->db->set('Tipo_auto_id',$actual['Tipo_auto_id']);
 		$this->db->set('Precio_dia',$actual['Precio_dia']);
 		$this->db->set('Estado_id',$actual['Estado_id']);
 		$this->db->set('caracteristica_id',$actual['caracteristica_id']);
 		$this->db->set('Fianza',$actual['Fianza']);
 		$this->db->where('Id_vehiculo',$actual['id']);
 		$this->db->update('vehiculo');
 			

 		}else{
			
 		$this->db->set('Marca_id',$actual['Marca_id']);
 		$this->db->set('Anio',$actual['Anio']);
 		$this->db->set('Placa',$actual['Placa']);
 		$this->db->set('Modelo',$actual['Modelo']);
 		$this->db->set('Color',$actual['Color']);
 		$this->db->set('Tipo_auto_id',$actual['Tipo_auto_id']);
 		$this->db->set('Precio_dia',$actual['Precio_dia']);
 		$this->db->set('Estado_id',$actual['Estado_id']);
 		$this->db->set('caracteristica_id',$actual['caracteristica_id']);
 		$this->db->set('Fianza',$actual['Fianza']);
 		$this->db->set('url_imagen',$actual['foto']);
 		$this->db->where('Id_vehiculo',$actual['id']);
 		$this->db->update('vehiculo');
 			

 		}
 	}

 	

 	
 	//-----------------------------------------------------------------------------------
 	public function mostrar_marca(){
 		$mar= $this->db->get('marca');
 		return $mar->result();
 	}

 	public function mostr_est(){
 		$es= $this->db->get('Estado_vehiculo');
 		return $es->result();

 }

 public function mostra_tipo(){
 	$ti=$this->db->get('tipo_auto');
 	return $ti->result();
 }

 public function mostrar_caracter(){
 $ter=$this->db->get('caracteristica');
 return $ter->result();

 }
public function ver_modal(){

	$mo=$this->db->get('vehiculo');
	return $mo->result();


}

public function mostrar_car_by_id($id, $tipo){	
	
$this->db->select('v.Id_vehiculo,m.Marca,v.Anio,v.Placa,v.Modelo,v.Color,t.TIpo_auto,v.Precio_dia,v.Fianza,e.Estado_vehiculo,c.aire,c.combustible,c.transmision,c.capacidad, v.url_imagen');
 		$this->db->join('marca m','v.Marca_id=m.Id_marca','inner');
 		$this->db->join('tipo_auto t','v.Tipo_auto_id=t.Id_tipo_auto','inner');
 		$this->db->join('estado_vehiculo e','v.Estado_id=e.Id_estado_vehiculo','inner');
 		$this->db->join('caracteristica c','v.caracteristica_id=c.idcaracteristica','inner');
 	    $this->db->where('t.TIpo_auto',$tipo);
 		$this->db->where('v.Id_vehiculo='.$id);
 		
 		$ver=$this->db->get('vehiculo v');
 		return $ver->result();
}

public function fto(){
	$f=$this->db->get('vehiculo');
	return $f->result();

}

public function stock_car(){
	$this->db->where('e.Estado_vehiculo','disponible');
		$this->db->select('v.Id_vehiculo,m.Marca,v.Anio,v.Placa,v.Modelo,v.Color,t.TIpo_auto,v.Precio_dia,v.Fianza,e.Estado_vehiculo,c.aire,c.combustible,c.transmision,c.capacidad, v.url_imagen');
 		$this->db->join('marca m','v.Marca_id=m.Id_marca','inner');
 		$this->db->join('tipo_auto t','v.Tipo_auto_id=t.Id_tipo_auto','inner');
 		$this->db->join('estado_vehiculo e','v.Estado_id=e.Id_estado_vehiculo','inner');
 		$this->db->join('caracteristica c','v.caracteristica_id=c.idcaracteristica','inner'); 		
 		$ver=$this->db->get('vehiculo v');
 		return $ver->result();
}

public function ingremar($dato){
	$this->db->set('Marca',$dato);
	$this->db->insert('marca');


}
public function elimimar($id){
	$this->db->where('Id_marca',$id);
	$this->db->delete('marca');
}
public function mmtto(){
	$re=$this->db->get('marca');
      return $re->result();

}
public function actualim($id){
	$this->db->where('Id_marca',$id['id']);
	$this->db->set('Marca',$id['mar']);
	$this->db->update('marca');
}



}

 ?>
<?php 
defined('BASEPATH') or exit ('No direct script access allowed');

class cliente_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}

	public function select_documento(){
		$ar=$this->db->get('documento');
		return $ar->result();
	}

	public function select_pais(){
		$ar=$this->db->get('pais');
		return $ar->result();
	}

	public function select_tipo(){
		$ar=$this->db->get('tipo_licencia');
		return $ar->result();
	}

	public function select_sexo(){
		$ar=$this->db->get('sexo');
		return $ar->result();	
	}

	public function insertar_cliente($in){
	$this->db->insert('cliente', $in);
	}

	public function mostrar_cliente(){
		$pro=$this->db->query('CALL `mostrar_cliente`();');
		return $pro->result();
	}

	public function llenar_cliente($Id_DUI){
		$this->db->select('Id_DUI, Nombre, Apellido, Edad, Fnacimiento, Sexo_id, Tipo_licencia_id, Documento_id, Numero_licencia, Pais_id, Telefono, Telefono_referencia');

		$this->db->where('Id_DUI',$Id_DUI);
		$rs=$this->this->db->get('cliente');
		return $rs->row();
	}

	public function actualizar_cliente($ac){
		$this->db->update('cliente', $ac);
	}

	public function eliminar_cliente($id){
		$this->db->where('Id_DUI', $id);
		$this->db->delete('cliente');
	}

}
 ?>}


<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

 class renta_model extends CI_model{


 	public function select_dui(){
 		$con=$this->db->get('cliente');
 		return $con->result();		

 	}

 	 	public function select_vehiculo(){
 		$asd=$this->db->get('vehiculo');
 		return $asd->result();		

 	}

 	 	 public function select_anexo(){
 		$c=$this->db->get('anexo');
 		return $c->result();	

 	}

 	public function insertar_renta($inser){
 		/*$this->db->set('DUI_id',$inser['DUI_id']);
 		$this->db->set('Vehiculo_id',$inser['Vehiculo_id']);
 		$this->db->set('Fecha_reserva',$inser['Fecha_reserva']);
 		$this->db->set('Fecha_devolucion',$inser['Fecha_devolucion']);
 		$this->db->set('Hora_reserva',$inser['Hora_reserva']);
 		$this->db->set('Hora_devolucion',$inser['Hora_devolucion']);
 		$this->db->set('Anexo_id',$inser['Anexo_id']);
 		$this->db->set('Fianza',$inser['Fianza']);
 		$this->db->set('Costo_total',$inser['Costo_total']);
 		$this->db->set('Hora',$inser['Hora']);
 		$this->db->set('Dia',$inser['Dia']);
 		$this->db->set('costo_alquiler',$inser['costo_alquiler']);*/

 		$this->db->insert('renta',$inser);
 		

 }

  	public function mostrar_renta(){
  		$this->db->select('c.Id_renta, c.DUI_id, b.Modelo, c.Fecha_reserva, c.Fecha_devolucion, c.Hora_reserva, c.Hora_devolucion, c.Fianza, c.costo_alquiler, d.Anexo, c.Hora, c.Dia, c.Costo_total');

		$this->db->join('cliente a','c.DUI_id=a.id_DUI','inner');
		$this->db->join('vehiculo b','c.Vehiculo_id=b.Id_vehiculo','inner');
		$this->db->join('anexo d','c.Anexo_id=d.Id_anexo','inner');
		
		$car=$this->db->get('renta c');
 		return $car->result();
 	}

 		public function eliminar_renta($id){
		$this->db->where('id_renta', $id);
		$this->db->delete('renta');
}

	public function actualizar_r($data){
		$this->db->set('DUI_id', $data['DUI_id']);
		$this->db->set('Vehiculo_id', $data['Vehiculo_id']);
		$this->db->set('Fecha_reserva', $data['Fecha_reserva']);
		$this->db->set('Fecha_devolucion', $data['Fecha_devolucion']);
		$this->db->set('Hora_reserva',$data['Hora_reserva']);
 		$this->db->set('Hora_devolucion',$data['Hora_devolucion']);
		$this->db->set('Anexo_id', $data['Anexo_id']);
		$this->db->set('Fianza',$data['Fianza']);
	    $this->db->set('Costo_total',$data['Costo_total']);
 		$this->db->set('Hora',$data['Hora']);
 		$this->db->set('Dia',$data['Dia']);
 		$this->db->set('costo_alquiler',$data['costo_alquiler']);
		$this->db->where('Id_renta', $data['id']);
		$this->db->update('renta',$data);
	}

	public function llenar_r($Id_renta){

		$this->db->select('DUI_id, Vehiculo_id, Fecha_reserva, Fecha_devolucion , Hora_reserva , Hora_devolucion , Anexo_id, fianza, Costo_total, Hora, Dia, costo_alquiler');

		$this->db->where('Id_renta', $Id_renta);
		$Llenar=$this->db->get('renta');
		return $Llenar->row();
	}

//

 	 	 public function mostrar_nombre($dis){
	$this->db->where('Id_DUI', $dis);
	$resultado=$this->db->get('cliente');
 	return $resultado->row()->Nombre ;


 	}

 	 	public function recu($idcar){

		$this->db->select('Id_vehiculo, Precio_dia, Modelo, Placa, Fianza');

 		$this->db->where('Id_vehiculo='.$idcar);
 		$ver=$this->db->get('vehiculo');
 		return $ver->row();
}

		public function cambiar_estado(){
		$this->db->set('DUI_id', '1');
		$this->db->update('vehiculo');
		}


}

 ?>

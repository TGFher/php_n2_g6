<?php	
defined('BASEPATH') OR exit ('No direct script access allowed');

Class Login_model extends CI_model{
	public function Validar_usuario($usuario, $clave)
	{	
		$this->db->where('usuario',$usuario);
		$this->db->where('clave', $clave);
		$validacion=$this->db->get('usuario');
		if($validacion->num_rows()>0){
			return $validacion->result();
		}
	}
	public function validar_user($user)
	{
		$this->db->where('usuario',$user);
		$result=$this->db->get('usuario');
		if($result->num_rows()>0)
		{
			return 1;
		}
		else{
			return 0;
		}
	}
	public function validar_clave($user,$clave)
	{
		$this->db->where('usuario',$user);
		$this->db->where('clave',$clave);
		$result=$this->db->get('usuario');
		if($result->num_rows()>0)
		{
			return 1;
		}
		else{
			return 0;
		}
	}
	public function validar_codigos($codigo)
	{
		$this->db->where('recu',$codigo);
		$result=$this->db->get('usuario');
		if($result->num_rows()>0)
		{
			return 1;
		}
		else{
			return 0;
		}
	}
	public function validar_mail($mail)
	{
		$this->db->where('correo',$mail);
		$result=$this->db->get('usuario');
		if($result->num_rows()>0)
		{
			return 1;
		}
		else{
			return 0;
		}
	}
	public function validar_codigo($codigo)
	{
		$this->db->where('recu',$codigo);
		$result=$this->db->get('usuario');
		if($result->num_rows()>0)
		{
			return $result->row();
		}
	}
	public function actualizar_clave ($clave){
		$this->db->set('clave', $clave['clave']);
		$this->db->set('recu', $clave['recu']);
		$this->db->where('idusuario', $clave['idusuario']);
		$this->db->update('usuario');
	}
// Funcion encargada de recuperar la contraseña.
	public function enviar_clave($correo) {
		$this->db->where('correo',$correo);
		$validacion=$this->db->get('usuario');
		if($validacion->num_rows()>0){
			return $validacion->result();
		}
	}
	public function llenarrol(){
		$var = $this->db->get('rol');
		return $var->result();

	}
	public function insertar_usuario($Insertar)
	{
		$this->db->set('nombre_usuario',$Insertar['nombre']);
		$this->db->set('usuario',$Insertar['usuario']);
		$this->db->set('clave',sha1(md5($Insertar['clave'])));
		$this->db->set('recu',$Insertar['recu']);
		$this->db->set('rol_id',$Insertar['rol']);
		$this->db->set('correo',$Insertar['correo']);
		$this->db->set('registro',$Insertar['registro']);
		$this->db->insert('usuario');
	}


	Public function Mostrar_usuario(){
		$this->db->select('a.idusuario,a.nombre_usuario,a.usuario,b.rol,a.correo');
		$this->db->join('rol b','a.rol_id=b.idrol','inner');
		$this->db->order_by("a.usuario", "asc");
		$mostrar1=$this->db->get('usuario a');
		return $mostrar1->result();
	}
	public function actualizar_usuario ($Actualizar){
		$this->db->set('nombre_usuario', $Actualizar['nombre']);
		$this->db->set('usuario', $Actualizar['usuario']);
		$this->db->set('clave', $Actualizar['clave']);
		$this->db->set('recu', $Actualizar['recu']);
		$this->db->set('rol_id', $Actualizar['rol']);
		$this->db->set('correo', $Actualizar['correo']);
		$this->db->set('registro', $Actualizar['registro']);
		$this->db->where('idusuario', $Actualizar['id']);
		$this->db->update('usuario');
	}

	public function Llenar_usuario($idusuario){

		$this->db->select('nombre_usuario, usuario, clave, recu, rol_id, correo, registro');

		$this->db->where('idusuario', $idusuario);
		$Llenar=$this->db->get('usuario');
		return $Llenar->row();
	}

	public function eliminar_usuario($id){
		$this->db->where('idusuario', $id);
		$this->db->delete('usuario');

	}
	//recuperar el nombre con un foreach
	public function mostrar_rol(){
		$rol = $this->db->get('rol');
		return $rol->result();
	}


}
-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-03-2019 a las 18:36:19
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `renta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anexo`
--

CREATE TABLE `anexo` (
  `Id_anexo` int(10) UNSIGNED NOT NULL,
  `Anexo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `anexo`
--

INSERT INTO `anexo` (`Id_anexo`, `Anexo`) VALUES
(1, 'Copia DUI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristica`
--

CREATE TABLE `caracteristica` (
  `idcaracteristica` int(11) NOT NULL,
  `aire` varchar(5) NOT NULL,
  `combustible` varchar(15) NOT NULL,
  `transmision` varchar(15) NOT NULL,
  `capacidad` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `caracteristica`
--

INSERT INTO `caracteristica` (`idcaracteristica`, `aire`, `combustible`, `transmision`, `capacidad`) VALUES
(1, 'si', 'gasolina', 'estandar', '5'),
(2, 'no', 'diesel', 'automatica', '4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_cliente`
--

CREATE TABLE `categoria_cliente` (
  `Id_categoria_cliente` int(10) UNSIGNED NOT NULL,
  `Categoria_cliente` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria_cliente`
--

INSERT INTO `categoria_cliente` (`Id_categoria_cliente`, `Categoria_cliente`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `Id_DUI` int(10) UNSIGNED NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Apellido` varchar(45) NOT NULL,
  `Edad` varchar(45) NOT NULL,
  `Fnacimiento` date NOT NULL,
  `Sexo_id` int(10) UNSIGNED NOT NULL,
  `TIpo_licencia_id` int(10) UNSIGNED NOT NULL,
  `Documento_id` int(10) UNSIGNED NOT NULL,
  `Numero_licencia` int(10) UNSIGNED NOT NULL,
  `Pais_id` int(10) UNSIGNED NOT NULL,
  `Telefono` varchar(45) NOT NULL,
  `Telefono_referencia` varchar(45) NOT NULL,
  `Categoria_cliente_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`Id_DUI`, `Nombre`, `Apellido`, `Edad`, `Fnacimiento`, `Sexo_id`, `TIpo_licencia_id`, `Documento_id`, `Numero_licencia`, `Pais_id`, `Telefono`, `Telefono_referencia`, `Categoria_cliente_id`) VALUES
(1, 'Walter Alexander ', 'Quintanilla Fuentes', '27', '2019-01-14', 1, 1, 1, 12435, 1, '23321111', '23322515', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `danio`
--

CREATE TABLE `danio` (
  `Id_danio` int(10) UNSIGNED NOT NULL,
  `TIpo_danio_id` int(10) UNSIGNED NOT NULL,
  `Observaciones` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento`
--

CREATE TABLE `documento` (
  `Id_documento` int(10) UNSIGNED NOT NULL,
  `Documento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `documento`
--

INSERT INTO `documento` (`Id_documento`, `Documento`) VALUES
(1, 'DUI'),
(2, 'Pasaporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `Id_empleados` int(10) UNSIGNED NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Apellido` varchar(45) NOT NULL,
  `Edad` varchar(45) NOT NULL,
  `Fnacimeinto` date NOT NULL,
  `DUI` varchar(45) NOT NULL,
  `Sexo_id` int(10) UNSIGNED NOT NULL,
  `Puesto_id` int(10) UNSIGNED NOT NULL,
  `Telefono` varchar(45) NOT NULL,
  `Telefono_emergencia` varchar(45) NOT NULL,
  `direccion` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_vehiculo`
--

CREATE TABLE `estado_vehiculo` (
  `Id_estado_vehiculo` int(10) UNSIGNED NOT NULL,
  `Estado_vehiculo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estado_vehiculo`
--

INSERT INTO `estado_vehiculo` (`Id_estado_vehiculo`, `Estado_vehiculo`) VALUES
(1, 'Arrendado'),
(2, 'Disponible'),
(3, 'En Mantenimiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_cliente`
--

CREATE TABLE `historial_cliente` (
  `Id_historial_cliente` int(10) UNSIGNED NOT NULL,
  `DUI_id` int(10) UNSIGNED NOT NULL,
  `Vehiculo_id` int(10) UNSIGNED NOT NULL,
  `Danio_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimiento`
--

CREATE TABLE `mantenimiento` (
  `Id_mantenimiento` int(10) UNSIGNED NOT NULL,
  `Fecha` date NOT NULL,
  `Observaciones` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `Id_marca` int(10) UNSIGNED NOT NULL,
  `Marca` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`Id_marca`, `Marca`) VALUES
(1, 'Toyota'),
(2, 'Honda');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `Id_pais` int(10) UNSIGNED NOT NULL,
  `Pais` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`Id_pais`, `Pais`) VALUES
(1, 'El Salvador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puesto_trabajo`
--

CREATE TABLE `puesto_trabajo` (
  `Id_puesto_trabajo` int(10) UNSIGNED NOT NULL,
  `Puesto` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puesto_trabajo`
--

INSERT INTO `puesto_trabajo` (`Id_puesto_trabajo`, `Puesto`) VALUES
(1, 'Gerente'),
(2, 'Atención al cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `renta`
--

CREATE TABLE `renta` (
  `Id_renta` int(10) UNSIGNED NOT NULL,
  `DUI_id` int(10) UNSIGNED NOT NULL,
  `Vehiculo_id` int(10) UNSIGNED NOT NULL,
  `Fecha_reserva` varchar(45) NOT NULL,
  `Fecha_entrega` varchar(45) NOT NULL,
  `Fecha_devolucion` varchar(45) NOT NULL,
  `Anexo_id` int(10) UNSIGNED NOT NULL,
  `fianza` varchar(5) NOT NULL,
  `costo_alquiler` varchar(5) NOT NULL,
  `Hora_reserva` time NOT NULL,
  `Hora_entrega` time NOT NULL,
  `Hora_devolucion` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `renta`
--

INSERT INTO `renta` (`Id_renta`, `DUI_id`, `Vehiculo_id`, `Fecha_reserva`, `Fecha_entrega`, `Fecha_devolucion`, `Anexo_id`, `fianza`, `costo_alquiler`, `Hora_reserva`, `Hora_entrega`, `Hora_devolucion`) VALUES
(1, 1, 13, '2019-03-04', '2019-03-15', '2019-03-22', 1, '50', '50', '12:00:00', '10:00:00', '10:00:00'),
(2, 1, 9, '0000-00-00', '0000-00-00', '0000-00-00', 1, '$60', '$60', '10:00:00', '12:00:00', '03:00:00'),
(3, 1, 13, '0000-00-00', '0000-00-00', '0000-00-00', 1, '$50', '$70', '12:00:00', '10:00:00', '12:00:00'),
(4, 1, 10, '03/04/2019', '03/05/2019', '03/06/2019', 1, '$75', '$100', '10:00:00', '10:00:00', '12:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idrol`, `rol`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sexo`
--

CREATE TABLE `sexo` (
  `Id_sexo` int(10) UNSIGNED NOT NULL,
  `Sexo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sexo`
--

INSERT INTO `sexo` (`Id_sexo`, `Sexo`) VALUES
(1, 'Masculino'),
(2, 'Femenino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_auto`
--

CREATE TABLE `tipo_auto` (
  `Id_tipo_auto` int(10) UNSIGNED NOT NULL,
  `TIpo_auto` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_auto`
--

INSERT INTO `tipo_auto` (`Id_tipo_auto`, `TIpo_auto`) VALUES
(1, 'sedán'),
(2, 'Cupé'),
(3, 'Hatchback'),
(4, 'Pick-up'),
(5, 'Camioneta'),
(6, 'Microbus');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_danio`
--

CREATE TABLE `tipo_danio` (
  `Id_tipo_danio` int(10) UNSIGNED NOT NULL,
  `Tipo_danio` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_licencia`
--

CREATE TABLE `tipo_licencia` (
  `Id_tipo_licencia` int(10) UNSIGNED NOT NULL,
  `Licencia` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_licencia`
--

INSERT INTO `tipo_licencia` (`Id_tipo_licencia`, `Licencia`) VALUES
(1, 'Nacional'),
(2, 'Extrangera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre_usuario` varchar(45) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `clave` text NOT NULL,
  `recu` varchar(50) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `correo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre_usuario`, `usuario`, `clave`, `recu`, `rol_id`, `correo`) VALUES
(1, 'Walter Quintanilla', 'walter', 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'c97b3c', 1, 'walter.quintanilla92@gmail.com'),
(3, 'gloria', 'glorita', 'caf1a3dfb505ffed0d024130f58c5cfa', '', 1, 'teficalderon01@gmail.com'),
(4, 'aSa', 'erd', '81dc9bdb52d04dc20036dbd8313ed055', '', 2, 'asdasda@gmail.com'),
(5, 'aSa', 'erd', '81dc9bdb52d04dc20036dbd8313ed055', '', 2, 'asdasda@gmail.com'),
(6, 'aSa', 'erd', '81dc9bdb52d04dc20036dbd8313ed055', '', 2, 'asdasda@gmail.com'),
(7, 'asdasd', 'ass', 'caf1a3dfb505ffed0d024130f58c5cfa', '', 2, 'asdasdasd@gmail.com'),
(8, 'ASasA', 'ASDAS', '202cb962ac59075b964b07152d234b70', '', 2, 'asdasda@gmail.com'),
(9, 'asdasd', 'asdasd', 'a8f5f167f44f4964e6c998dee827110c', '', 1, 'asdas@asda'),
(10, 'dsfdsf', 'sdfsdf', 'd58e3582afa99040e27b92b13c8f2280', '', 2, 'sdfsdf@asd.com'),
(11, 'asdasd', 'asdasd', 'a8f5f167f44f4964e6c998dee827110c', '', 1, 'asdas@asda'),
(12, 'asdasd', 'asdas', 'a8f5f167f44f4964e6c998dee827110c', '', 2, 'asdas@asda'),
(13, 'asdasd', 'asdasd', 'a809187cf02c6f33eb708b2600525314f721a77b', '', 2, 'asdas@asda'),
(14, 'asdasd', 'asdasd', 'a8f5f167f44f4964e6c998dee827110c', '', 2, 'asdas@asda'),
(15, 'asdasd', 'asdasd', 'a8f5f167f44f4964e6c998dee827110c', '', 2, 'asdas@asda'),
(16, 'asdasd', 'asdas', '0aa1ea9a5a04b78d4581dd6d17742627', '', 2, 'asdas@asda'),
(17, 'asdas', 'asdas', '0aa1ea9a5a04b78d4581dd6d17742627', '', 2, 'asdas@asda'),
(18, 'Fernando Trejo', 'fer', '202cb962ac59075b964b07152d234b70', '123', 1, 'ferjo.trejo@gmail.com'),
(19, 'Omar Zahir Vasquez Menjivar', 'omar', 'e10adc3949ba59abbe56e057f20f883e', '123456', 1, 'zahir.menjivar@gmail.com'),
(20, 'zahir menjivar', 'menjizar', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', '1234', 1, 'zahir.menjivar@gmail.com'),
(21, 'Veronica Fernanda', 'verito', 'adcd7048512e64b48da55b027577886ee5a36350', '505cbe', 1, 'felicityfornaun2019@gmail.com'),
(25, 'Saúl Álvarez Pacheco', 'saulap', 'b4894d40e55784c525e21bccc4377fe756a52630', 'ae71d0', 1, 'alvarezs553@gmail.com'),
(26, 'Jose Isaias', 'js', 'df83f7fa4ab419db2dcd4384684f3b637e7fc331', '5adbfc', 1, 'asdas@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `Id_vehiculo` int(10) UNSIGNED NOT NULL,
  `Marca_id` int(10) UNSIGNED NOT NULL,
  `Anio` int(45) NOT NULL,
  `Placa` varchar(45) NOT NULL,
  `Modelo` varchar(45) NOT NULL,
  `Color` varchar(45) NOT NULL,
  `Tipo_auto_id` int(10) UNSIGNED NOT NULL,
  `Precio_dia` varchar(45) NOT NULL,
  `Estado_id` int(10) UNSIGNED NOT NULL,
  `caracteristica_id` int(11) NOT NULL,
  `url_imagen` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`Id_vehiculo`, `Marca_id`, `Anio`, `Placa`, `Modelo`, `Color`, `Tipo_auto_id`, `Precio_dia`, `Estado_id`, `caracteristica_id`, `url_imagen`) VALUES
(1, 1, 2009, 'p1321313', 'corolla', 'rojo', 1, '10', 1, 2, ''),
(2, 1, 2009, 'p-hgsdh', 'corolla', 'rojo', 1, '25', 2, 2, ''),
(3, 2, 2013, 'p-walter', 'corolla', 'anaranjado', 4, '25', 2, 1, ''),
(9, 1, 2012, 'p-zahir_@', 'tercel', 'azul', 1, '15', 2, 1, ''),
(10, 1, 2009, 'p-zahir_@', 'corolla', 'rojo', 1, '25', 1, 1, ''),
(11, 2, 2019, 'p1321313', 'corolla', 'verde', 2, '25', 1, 1, ''),
(13, 1, 2017, 'xfdhgdshg', 'vgjnfj', 'fhfhfr', 2, 'fthfr', 2, 1, ''),
(14, 1, 2012, 'p20156', 'corolla', 'Verde', 4, '10', 2, 2, ''),
(15, 2, 2016, 'p201928', 'Civic', 'Gris', 6, '25', 1, 2, ''),
(18, 1, 2009, 'lk98451', 'corolla', 'azul', 2, '100', 2, 1, 'ilya-yakover.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anexo`
--
ALTER TABLE `anexo`
  ADD PRIMARY KEY (`Id_anexo`);

--
-- Indices de la tabla `caracteristica`
--
ALTER TABLE `caracteristica`
  ADD PRIMARY KEY (`idcaracteristica`);

--
-- Indices de la tabla `categoria_cliente`
--
ALTER TABLE `categoria_cliente`
  ADD PRIMARY KEY (`Id_categoria_cliente`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`Id_DUI`),
  ADD KEY `cliente_sexo` (`Sexo_id`),
  ADD KEY `cliente_tipo_licencia` (`TIpo_licencia_id`),
  ADD KEY `cliente_documento` (`Documento_id`),
  ADD KEY `cliente_pais` (`Pais_id`),
  ADD KEY `cliente_categoria` (`Categoria_cliente_id`);

--
-- Indices de la tabla `danio`
--
ALTER TABLE `danio`
  ADD PRIMARY KEY (`Id_danio`),
  ADD KEY `danio_tipo_danio` (`TIpo_danio_id`);

--
-- Indices de la tabla `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`Id_documento`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`Id_empleados`),
  ADD KEY `empleados_sexo` (`Sexo_id`),
  ADD KEY `empleados_puesto` (`Puesto_id`);

--
-- Indices de la tabla `estado_vehiculo`
--
ALTER TABLE `estado_vehiculo`
  ADD PRIMARY KEY (`Id_estado_vehiculo`);

--
-- Indices de la tabla `historial_cliente`
--
ALTER TABLE `historial_cliente`
  ADD PRIMARY KEY (`Id_historial_cliente`),
  ADD KEY `historial_dui` (`DUI_id`),
  ADD KEY `historial_vehiculo` (`Vehiculo_id`),
  ADD KEY `historial_danio` (`Danio_id`);

--
-- Indices de la tabla `mantenimiento`
--
ALTER TABLE `mantenimiento`
  ADD PRIMARY KEY (`Id_mantenimiento`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`Id_marca`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`Id_pais`);

--
-- Indices de la tabla `puesto_trabajo`
--
ALTER TABLE `puesto_trabajo`
  ADD PRIMARY KEY (`Id_puesto_trabajo`);

--
-- Indices de la tabla `renta`
--
ALTER TABLE `renta`
  ADD PRIMARY KEY (`Id_renta`),
  ADD KEY `renta_dui` (`DUI_id`),
  ADD KEY `renta_vehiculo` (`Vehiculo_id`),
  ADD KEY `renta_anexo` (`Anexo_id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `sexo`
--
ALTER TABLE `sexo`
  ADD PRIMARY KEY (`Id_sexo`);

--
-- Indices de la tabla `tipo_auto`
--
ALTER TABLE `tipo_auto`
  ADD PRIMARY KEY (`Id_tipo_auto`);

--
-- Indices de la tabla `tipo_danio`
--
ALTER TABLE `tipo_danio`
  ADD PRIMARY KEY (`Id_tipo_danio`);

--
-- Indices de la tabla `tipo_licencia`
--
ALTER TABLE `tipo_licencia`
  ADD PRIMARY KEY (`Id_tipo_licencia`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `rol_id` (`rol_id`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`Id_vehiculo`),
  ADD KEY `vehiculo_marca` (`Marca_id`),
  ADD KEY `vehiculo_tipo_auto` (`Tipo_auto_id`),
  ADD KEY `vehiculo_estado_vehiculo` (`Estado_id`),
  ADD KEY `caracteristica_id` (`caracteristica_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anexo`
--
ALTER TABLE `anexo`
  MODIFY `Id_anexo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `caracteristica`
--
ALTER TABLE `caracteristica`
  MODIFY `idcaracteristica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `categoria_cliente`
--
ALTER TABLE `categoria_cliente`
  MODIFY `Id_categoria_cliente` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `Id_DUI` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `danio`
--
ALTER TABLE `danio`
  MODIFY `Id_danio` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `documento`
--
ALTER TABLE `documento`
  MODIFY `Id_documento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `Id_empleados` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estado_vehiculo`
--
ALTER TABLE `estado_vehiculo`
  MODIFY `Id_estado_vehiculo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `historial_cliente`
--
ALTER TABLE `historial_cliente`
  MODIFY `Id_historial_cliente` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mantenimiento`
--
ALTER TABLE `mantenimiento`
  MODIFY `Id_mantenimiento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `Id_marca` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `Id_pais` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `puesto_trabajo`
--
ALTER TABLE `puesto_trabajo`
  MODIFY `Id_puesto_trabajo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `renta`
--
ALTER TABLE `renta`
  MODIFY `Id_renta` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sexo`
--
ALTER TABLE `sexo`
  MODIFY `Id_sexo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_auto`
--
ALTER TABLE `tipo_auto`
  MODIFY `Id_tipo_auto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tipo_danio`
--
ALTER TABLE `tipo_danio`
  MODIFY `Id_tipo_danio` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_licencia`
--
ALTER TABLE `tipo_licencia`
  MODIFY `Id_tipo_licencia` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  MODIFY `Id_vehiculo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_categoria` FOREIGN KEY (`Categoria_cliente_id`) REFERENCES `categoria_cliente` (`Id_categoria_cliente`),
  ADD CONSTRAINT `cliente_documento` FOREIGN KEY (`Documento_id`) REFERENCES `documento` (`Id_documento`),
  ADD CONSTRAINT `cliente_pais` FOREIGN KEY (`Pais_id`) REFERENCES `pais` (`Id_pais`),
  ADD CONSTRAINT `cliente_sexo` FOREIGN KEY (`Sexo_id`) REFERENCES `sexo` (`Id_sexo`),
  ADD CONSTRAINT `cliente_tipo_licencia` FOREIGN KEY (`TIpo_licencia_id`) REFERENCES `tipo_licencia` (`Id_tipo_licencia`);

--
-- Filtros para la tabla `danio`
--
ALTER TABLE `danio`
  ADD CONSTRAINT `danio_tipo_danio` FOREIGN KEY (`TIpo_danio_id`) REFERENCES `tipo_danio` (`Id_tipo_danio`);

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `empleados_puesto` FOREIGN KEY (`Puesto_id`) REFERENCES `puesto_trabajo` (`Id_puesto_trabajo`),
  ADD CONSTRAINT `empleados_sexo` FOREIGN KEY (`Sexo_id`) REFERENCES `sexo` (`Id_sexo`);

--
-- Filtros para la tabla `historial_cliente`
--
ALTER TABLE `historial_cliente`
  ADD CONSTRAINT `historial_cliente_ibfk_1` FOREIGN KEY (`DUI_id`) REFERENCES `cliente` (`Id_DUI`),
  ADD CONSTRAINT `historial_danio` FOREIGN KEY (`Danio_id`) REFERENCES `danio` (`Id_danio`);

--
-- Filtros para la tabla `renta`
--
ALTER TABLE `renta`
  ADD CONSTRAINT `renta_anexo` FOREIGN KEY (`Anexo_id`) REFERENCES `anexo` (`Id_anexo`),
  ADD CONSTRAINT `renta_ibfk_1` FOREIGN KEY (`DUI_id`) REFERENCES `cliente` (`Id_DUI`),
  ADD CONSTRAINT `renta_vehiculo` FOREIGN KEY (`Vehiculo_id`) REFERENCES `vehiculo` (`Id_vehiculo`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`idrol`);

--
-- Filtros para la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD CONSTRAINT `vehiculo_estado_vehiculo` FOREIGN KEY (`Estado_id`) REFERENCES `estado_vehiculo` (`Id_estado_vehiculo`),
  ADD CONSTRAINT `vehiculo_ibfk_1` FOREIGN KEY (`caracteristica_id`) REFERENCES `caracteristica` (`idcaracteristica`),
  ADD CONSTRAINT `vehiculo_marca` FOREIGN KEY (`Marca_id`) REFERENCES `marca` (`Id_marca`),
  ADD CONSTRAINT `vehiculo_tipo_auto` FOREIGN KEY (`Tipo_auto_id`) REFERENCES `tipo_auto` (`Id_tipo_auto`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
